\chapter{Configuración del Entorno} \label{anexo1}
\label{anexo:configuracion}
\noindent
\drop{E}{n} este anexo, se mostrará de forma más completa y exhaustiva, cuáles son los pasos que han de darse para configurar el entorno, tanto hardware como software, necesario para la ejecución de este proyecto. Del mismo modo que en el capítulo \ref{chap:objetivos} se han identificado dos dimensiones, hardware y software del entorno, en este anexo se desarrollarán cada una de ellas más detenidamente. \\

\section{Configuración del entorno software}
A continuación se analizan detenidamente las diferentes herramientas software que han de configurarse correctamente, tal y como se explica a posteriori, para el correcto funcionamiento del proyecto completo. 

\begin{itemize}
\item \textbf{Configuración de AVRDUDE:} Es el acrónimo de AVR Downloader Uploader. Es un programa que nos permite descargar y escribir en la memoria de los chips de microcontroladores AVR Atmel. Este programa nos permite, por tanto, programar la memoria FLASH y EEPROM de dichos chips y provee de soporte a diferentes protocolos de programación para facilitar esta tarea, permitiendo la programación y el bloqueo de bits. AVRDUDE puede ser ejecutado desde la línea de comandos, modo que se usa principalmente para leer o escribir en la memoria del microcontrolador, o en un modo interactivo, utilizado principalmente para explorar el contenido de la memoria o modificar bytes de la memoria EEPROM, bloquear bits...\\
La primera fase de configuración del entorno software, consistirá en descargar e instalar en la máquina AVRDUDE para, como se verá en la configuración del entorno hardware, poder actualizar el firmware del controlador OHC y de los Kilobots. Para instalar AVRDUDE, se ejecutará desde la consola el siguiente comando:

\lstdefinestyle{consola}
   {basicstyle=\scriptsize\bf\ttfamily,
    backgroundcolor=\color{gray75},
   }
   
    
\begin{listing}[style=consola, numbers=none]
$ sudo apt-get install avrdude
\end{listing}

\item \textbf{Instalación de AVR-Libc:} Se trata de un proyecto de software libre que provee de una librería con funciones de alto nivel en lenguaje C para utilizar microcontroladores AVR. Esta librería proporcionará el conjunto de funciones necesarias para programar el controlador OHC. El comando para instalar esta librería es el que se muestra a continuación.

\begin{listing}[style=consola, numbers=none]
$ sudo apt-get install avr-libc gcc-avr avrdude
\end{listing}
 

\item \textbf{kilolib-master:} Se trata de una librería desarrollada por A.Cornejo, de la universidad de Harvard. Esta librería proporciona la \acs{API} necesaria para trabajar con los kilobots y utiliza en la implementación de dichas funciones, otras provistas por AVR-Libc, por lo que para usarla, se requiere la instalación del componente descrito anteriormente. El código de esta librería está disponible en el enlace: \url{https://github.com/acornejo/kilolib}. 

\item \textbf{kilobot-labs-master:} Conjunto de ejemplos desarrollados por A.Cornejo donde se da una visión panorámica con ejemplos prácticos de las capacidades de los Kilobots y el uso de su modelo de programación y arquitectura. Estos archivos servirán para ejecutar los primeros programas en los robots y familiarizarse con su entorno de programación. Estos ejemplos, son de código libre y pueden obtenerse en: \url{https://github.com/acornejo/kilobot-labs}.

\item \textbf{Kilogui:} Interfaz de usuario que permite la comunicación entre el controlador \acs{OHC} y los Kilobots. A través de esta interfaz, podemos dar diversas órdenes al controlador, que a su vez emite a los kilobots, posibilitando el control y la monitorización de la ejecución de los programas a través de una interfaz gráfica, que se muestra en \ref{fig:kilogui}. 
\begin{figure}
  \centering
    \includegraphics[width=0.35\textwidth,height=0.35\textheight]{Figuras/kilogui}
  \caption{Interfaz gráfica Kilogui}
  \label{fig:kilogui}
\end{figure}
En primer lugar, y si analizamos esta ventana siguiendo un enfoque top-down, tenemos la selección del dispositivo, en la que se muestran las opciones FTDI y Serial. En esta elección, no es necesario marcar ninguna opción, ya que una vez el entorno hardware esté configurado, la opción \acs{FTDI} aparecerá marcada, pues el controlador OHC se conecta a la máquina mediante un puerto \acs{USB}. \\
En una segunda fase, una vez que se ha seleccionado el dispositivo, la ventana de kilogui permite cargar un programa en los kilobots. Para ello, dispone de un botón que permite seleccionar el archivo .hex que contiene el programa que se desea ejecutar. Una vez se ha seleccionado el archivo, se debe proceder de la siguiente forma:
\begin{enumerate}
\item \textbf{Bootload:} Una vez que se ha seleccionado el archivo que contiene el programa que se va a ejecutar, se acciona el botón Bootload que inicia el sistema de arranque de los Kilobots y los prepara para la carga del programa. Este paso debe realizarse cada vez que un programa vaya a cargarse. De forma empírica, se puede ver el proceso de arranque en cada Kilobot, que pondrá su \acs{LED} en color azul en señal de que el proceso de arranque se ha completado y el robot se encuentra a la espera de cargar un determinado programa.
\item \textbf{Upload:} Esta orden permite el envío de un programa a los Kilobots, que se encuentran ya arrancados y a la espera de un programa. De forma empírica, los robots emiten un parpadeo en su LED de color amarillo durante el proceso de carga, cesando éste una vez que el programa se ha cargado adecuadamente. Este proceso puede tardar unos minutos. 
\end{enumerate}
Una vez que el programa ha sido cargado en los Kilobots, la interfaz de Kilogui permite enviar una serie de comandos a los robots, disponiendo de un conjunto de botones que transmiten una orden determinada. El conjunto de comandos que se pueden realizar sobre los Kilobots se muestran en la tabla \ref{Comandos Kilobots} 
\input{tables/Comandos-Kilobots.tex}
Las operaciones Serial Input y calibración se verán detenidamente en la configuración del entorno hardware.
\end{itemize}

\section{Configuración del entorno hardware}
En este apartado se tratará con mayor detalle y minuciosidad cómo se ha configurado el entorno hardware sobre el que se ha desarrollado y ejecutado el proyecto. Para ello se seguirá un enfoque \emph{step by step} donde se mostrarán cuáles han sido los pasos que se han dado para la configuración del entorno hardware y en qué orden.
\begin{itemize}
\item \textbf{Actualización del firmware del controlador OHC:} En primer lugar, para poder utilizar los recursos software que provee la Universidad de Harvard, y concretamente el equipo de investigación en sistemas auto-organizables, deberemos actualizar el firmware tanto del controlador OHC como de los Kilobots. Esto permitirá hacer uso de la librería kilolib y de los ejemplos de kilobot-labs. A continuación, se describen los pasos para actualizar el firmware del controlador.
\begin{enumerate}
\item \textbf{Descarga del archivo con la última versión del firmware: }En primer lugar, se procede a la descarga del archivo con la última versión del firmware del controlador OHC, el cuál se puede encontrar en \url{https://raw.githubusercontent.com/acornejo/kilogui/binaries/controller.hex}. 
\item \textbf{Conexión del controlador OHC a la máquina: }Después de la descarga del archivo \emph{.hex} que contiene el firmware del controlador, se conecta éste mediante un puerto USB al PC, asegurándonos de que el jumper de programación del OHC está insertado en sus pines correspondientes(los dos de la izquierda de los tres pines de programación existentes). La conexión debe realizarse tal y como se indica en la figura \ref{fig:OHC}. 
\begin{figure}
  \centering
    \includegraphics[width=0.5\textwidth,height=0.3\textheight]{Figuras/OHC}
  \caption{Conexión del controlador OHC}
  \label{fig:OHC}
\end{figure}
\item \textbf{Actualización del Firmware:} Abriendo un terminal y situándonos bajo el directorio en el que se encuentra el fichero con la última versión del firmware del controlador, se escribe el siguiente comando que utiliza AVRDUDE para actualizar el firmware del controlador, escribiendo en la memoria de éste el contenido del fichero descargado en el paso uno.
\begin{listing}[style=consola, numbers=none]
$     avrdude -p m328  -P usb -c avrispmkII -U "flash:w:controller.hex:i"
\end{listing}
La sintaxis de este comando, con sus respectivas opciones es la siguiente: 
\begin{itemize}
\item \textbf{-p:} Esta opción permite seleccionar y especificar el dispositivo AVR. 
\item \textbf{-P:} Opción que especifica el puerto de conexión del dispositivo.
\item \textbf{-c:} Especifica el tipo de programación del dispositivo, por lo que el fichero de actualizacion debe estar escrito conforme a este lenguaje y modelo de programacion. 
\item \textbf{-U:} Especificación de la operación que se va a realizar en la memoria, donde flash, indica el tipo de memoria, ``w'' indica que se va a realizar una operación de escritura sobre la memoria del dispositivo y a continuación aparece el archivo que se va a escribir  sobre el mismo, con su formato ``Intel Hex'' por lo que se escribe ``i''. 
\end{itemize}
Una actualización correcta del firmware producirá la salida de consola que se puede apreciar en \ref{fig:Act-OHC}. 
\begin{figure}
  \centering
    \includegraphics[width=0.7\textwidth,height=0.3\textheight]{Figuras/Act-OHC}
  \caption{Actualización del Firmware del controlador OHC}
  \label{fig:Act-OHC}
\end{figure}
\end{enumerate}
\item \textbf{Actualización del firmware de los Kilobots:} Una vez que se ha actualizado el firmware del controlador OHC, el siguiente paso será actualizar el firmware de los Kilobots, de manera que éste se encuentre en consonancia con el del controlador, y de este modo, se pueda producir la comunicación entre ambos. Para llevar a cabo la actualización, necesitaremos el controlador OHC y el cable ICSP que se incluye con los Kilobots, que permite la lectura, escritura, verificación y depuración del software incluido en los microcontroladores. Esta tecnología, permite por tanto, programar estos componentes sin necesidad de extraerlos del circuito, produciendo un ahorro importante de tiempo y de trabajo. A continuación, se muestran los pasos que hay que llevar a cabo para la actualización del firmware de cada Kilobot:
\begin{enumerate}
\item \textbf{Descarga del archivo con la última versión del firmware:} De forma análoga a como se realizaba la actualización del firmware del OHC, para actualizar el firmware de los Kilobots el primer paso será la descarga del fichero .hex que contiene valores hexadecimales que codifican los datos y su offset o dirección de memoria, posibilitando de esta forma la programación de microcontroladores. El archivo se encuentra disponible en \url{https://raw.githubusercontent.com/acornejo/kilogui/binaries/bootloader.hex}.
\item \textbf{Conexión del controlador OHC a la máquina:} También de forma análoga a la actualización del firmware del controlador, será necesario conectar este a la máquina tal y como se vio en la figura \ref{fig:OHC}, con la diferencia de que para la actualización del firmware de los Kilobots, será necesario que el jumper de programación del controlador OHC esté insertado en los dos pines de la derecha de los tres pines de programación. 
\item \textbf{Encendido de los Kilobots:} A continuación, será necesario la puesta en funcionamiento del Kilobot al que se quiere actualizar el firmware, para lo que se añadirá al mismo el jumper de energía tal y como se muestra en la figura \ref{fig:powkilobot}.
\begin{figure}
  \centering
    \includegraphics[width=0.4\textwidth,height=0.2\textheight]{Figuras/kilobot-power}
  \caption{Encendido de los Kilobots}
  \label{fig:powkilobot}
\end{figure}
\end{enumerate}
\item \textbf{Conexión del cable ICSP:} Una vez se ha encendido el Kilobot, será necesario conectar el cable ICSP al controlador OHC y al Kilobot, asegurando que está bien conectado, sobre todo al Kilobot, para ello, se presionará para garantizar la conexión durante la actualización. Este procedimiento se puede ver en las figuras \ref{fig:conexion-ohc} y \ref{fig:conexion-kilobot}.
\begin{figure}
  \centering
    \includegraphics[width=0.5\textwidth,height=0.25\textheight]{Figuras/ohc-connect}
  \caption{Conexión del cable ICSP al Controlador OHC}
  \label{fig:conexion-ohc}
\end{figure}
\begin{figure}
  \centering
    \includegraphics[width=0.45\textwidth,height=0.2\textheight]{Figuras/kilobot-connect}
  \caption{Conexión del cable ICSP a los Kilobots}
  \label{fig:conexion-kilobot}
\end{figure}
\item \textbf{Actualización del Firmware:} El último paso es muy similar al que se da al actualizar el firmware del controlador OHC. Para ello se abrirá una terminal en el mismo directorio en el que se encuentra el archivo descargado con el firmware actual y se ejecutaŕa AVRDUDE para escribir en la memoria de los Kilobots esta última versión del firmware. El comando a utilizar es el siguiente.\\
\begin{listing}[style=consola, numbers=none]
$     avrdude -p m328  -P usb -c avrispmkII -U "flash:w:bootloader.hex:i"
\end{listing}
Como se puede comprobar, la sintaxis del comando es muy similar al anterior, solo que ahora el fichero que se especifica en la operación de escritura es el que contiene el firmware del Kilbot y el dispositivo que se selecciona ya no es el controlador, sino el propio Kilobot. Sin embargo, el resto de opciones son las mismas que se especificaron anteriormente, pues el método de programación y el puerto por el cual se establece la conexión permanecen inalterados. \\
La salida que se obtiene al ejecutar este comando y que indica la actualización del firmware es la que se muestra en la figura \ref{fig:Act-Kilobot}. Se recomienda presionar la conexión del cable ICSP al Kilobot durante la actualización para evitar fallos de conexión que provocarían el aborto de la actualización. Cuando la actualización se ha completado, el Kilobot vibrará, en señal de que la actualización ha sido correcta. 
\begin{figure}
  \centering
    \includegraphics[width=0.75\textwidth,height=0.35\textheight]{Figuras/Act-Kilobot}
  \caption{Actualización del Firmware de los Kilobots}
  \label{fig:Act-Kilobot}
\end{figure}
\item \textbf{Carga de los robots:} Tal y como se vio en la descripción del entorno hardware realizada en la sección \ref{chap:objetivos} los Kilobots están alimentados por una batería, que por ende, necesitará ser cargada para realizar las pruebas y poder ejecutar el sistema completo. \\
El cargador de Kilobots está formado por dos barras metálicas de inducción, situadas una encima de la otra, que poseen en su extremo un indicador de color rojo o negro. Estas barras metálicas están unidas en sus extremos a unas placas que forman una ``K'' en referencia al K-Team, empresa que distribuye y fabrica esta arquitectura robótica. Para cargar los Kilobots, se deberán seguir detenidamente los siguientes pasos:
\begin{enumerate}
\item Colocar el cargador en una superficie plana, de forma que la barra que posee el indicador rojo esté abajo y la que posee el indicador negro esté arriba. Es muy importante respetar este orden, pues si la posición de las barras no se respeta, es posible que la carga no se produzca e incluso se produzcan daños en los Kilobots.
\item Una vez que el cargador está colocado de forma adecuada, se enchufará a la corriente el cargador, conectando su cable a la luz y al conector de 6V prestando atención a la polarización, representando el signo + fuera y el signo - dentro. En caso de no respetar la polarización, es posible que la carga no se produzca e incluso que se produzcan daños en los Kilobots, de igual forma que en el paso anterior. 
\item Una vez que el cargador ya está listo para recibir a los Kilobots, encendemos aquellos Kilobots que se quieren cargar, tal y como se mostró en la figura \ref{fig:powkilobot}.
\item Colocar los Kilobots a cargar tal y como se explicita en la figura \ref{fig:cargador}. La barra con el indicador rojo contacta con las piernas de los robots y es la barra positiva.
 \begin{figure}
  \centering
    \includegraphics[width=1\textwidth,height=0.35\textheight]{Figuras/cargador}
  \caption{Cargador Kilobots}
  \label{fig:cargador}
\end{figure}
\item Utilizando la interfaz gráfica kilogui, pulsaremos el botón Voltage, que aparece en la figura \ref{fig:kilogui}. El robot en este momento comenzará a cargarse, emitiendo una luz de color azul. Cuando la carga se haya completado, emitirá una luz verde 
\item Cuando la carga ha finalizado, se retirarán los Kilobots del cargador y se utilizarán normalmente. Se recomienda que una vez se hayan retirado los robots, se pulse el botón Pause de la interfaz de Kilogui y se proceda entonces con el funcionamiento normal de los Kilobots.
\end{enumerate} 
\item \textbf{Calibración de los Kilobots:} Los Kilobots utilizan para su locomoción sus patas, que se mueven mediante unos motores de vibración. Este tipo de locomoción es conocida como stick-slip o locomoción mediante oscilaciones de relajación. Debido a las diferencias de fabricación inevitables entre dos Kilobots distintos, la potencia o energía requerida para garantizar un movimiento adecuado hacia delante y unos giros completos, es diferente. Por otra parte, esta energía también varía normalmente en función de la superficie sobre la que se sitúen los Kilobots. Estas restricciones físicas y del entorno hacen necesaria la calibración manual e individual de los valores de potencia que cada Kilobot utilizará para realizar sus movimientos. Para definir estos valores, elegiremos en la interfaz gráfica de Kilogui, que se puede ver en la figura \ref{fig:kilogui} la opción de Calibration, obteniendo la ventana que se muestra en la figura \ref{fig:calibracion}.
\begin{figure}
  \centering
    \includegraphics[width=0.65\textwidth,height=0.35\textheight]{Figuras/calibracion}
  \caption{Opciones de Calibración de los Kilobots}
  \label{fig:calibracion}
\end{figure}
A continuación, se muestran detalladamente cada uno de los valores que se han de definir en esta ventana de configuración.
\begin{enumerate}
\item \textbf{Unique ID:} Identificador único que nos servirá para distinguir a un Kilobot de otro a la hora de implementar los diferentes programas que compondrán el sistema.
\item \textbf{Turn left:} Valor de potencia entre 0 y 255 que indicará la potencia que utilizará el Kilobot cada vez que efectúe un giro a la izquierda. Por norma general, los valores definidos suelen oscilar entre 60 y 75, aunque como se dijo anteriormente, estos valores dependerán del robot y la superficie.
\item \textbf{Turn Right:} Valor de potencia entre 0 y 255 que indicará la potencia que utilizará el Kilobot cada vez que efectúe un giro a la derecha. Los valores definidos para éste giro serán similares a los del giro a la izquierda, aunque podrán variar, en función de las características de la superficie y del robot en particular.  
\item \textbf{Go Straight:} Valor de potencia definido a su vez por dos valores que definirán la potencia utilizada por el Kilobot para moverse hacia delante. Los dos valores que componen este parámetro son valores de giro a la izquierda y giro a la derecha, pues cuando se produce a su vez un giro a la izquierda y a la derecha, se genera un movimiento en línea recta. Por este motivo, los valores de partida que utilizaremos serán los mismos que se definieron para los giros a la izquierda y la derecha respectivamente, aunque pueden variar en función de la superficie. Normalmente, estos valores oscilan entre dos y diez unidades menores que los valores  definidos para cada uno de los giros. 
\end{enumerate}
Cada uno de estos parámetros pueden probarse una vez que se han definido utilizando el botón test. De esta forma, mediante ensayos prueba-error, se puede llegar a unos valores óptimos de locomoción para cada Kilobot. El valor Unique ID no se puede probar directamente, hasta que no se utilice en algun ejemplo concreto. \\
Una vez que se han definido los valores deseados en cada uno de los parámetros, utilizaremos el botón Save que proporciona la interfaz de Kilogui para guardar en la memoria EEPROM de los Kilobots dichos valores, de manera que cuando realicemos diferentes ejemplos, los robots utilicen dichos valores para ejecutar sus movimientos. 
\item \textbf{Depuración de los Kilobots:} Con el objetivo de facilitar la tarea de programación, el K-Team provee de una salida serie a los Kilobots para permitir la depuración. Para utilizar el modo de depuración se debe conectar un cable de dos pines desde la salida serie del Kilobot, a la entrada serie disponible en el controlador. A través de este cable serie podremos transmitir mensajes utilizando el archivo debug.h. Los pasos a seguir para utilizar el modo de depuración se muestran a continuación:
\begin{enumerate}
\item Conectar el cable de programación serie al controlador OHC tal y como se muestra en la figura \ref{fig:deb-OHC}.
\begin{figure}[h]
  \centering
    \includegraphics[width=0.5\textwidth,height=0.25\textheight]{Figuras/ohc-debug}
  \caption{Conexión del cable serie de programación al controlador OHC}
  \label{fig:deb-OHC}
\end{figure}
\item Conectar el cable de programación serie al Kilobot según se explicita en la figura \ref{fig:deb-kilobot}.
\begin{figure}[h]
  \centering
    \includegraphics[width=0.35\textwidth,height=0.25\textheight]{Figuras/kilobot-debug}
  \caption{Conexión del cable serie de programación al Kilobot}
  \label{fig:deb-kilobot}
\end{figure}
\end{enumerate}
\end{itemize}

\section{Configuración del entorno de ejecución}
En esta sección se mostrará detenidamente y paso a paso, cómo se realiza la carga y ejecución de los programas en los Kilobots. Para ello, a continuación se da el enfoque general, que contiene todos y cada uno de los pasos y el orden de ejecución de los mismos. Después, se describirá detalladamente cada uno de los pasos y se mostrará el resultado de la ejecución de cada uno de ellos. 
\begin{enumerate}
\item Compilación del programa y generación del archivo .hex
\item Selección en la interfaz de Kilogui del programa a ejecutar
\item Carga y ejecución del programa en la arquitectura 
\end{enumerate}
Cada uno de estos pasos, ha de desarrollarse como se expone:
\begin{enumerate}
\item \textbf{Compilación del programa y generación del archivo .hex:} Una vez que ya se ha escrito el programa que se quiere ejecutar en los Kilobots, utilizando para ello la librería kilolib y el archivo blank.c, el siguiente paso será compilar el programa escrito, el cual implicará la generación de un archivo .hex que contendrá las instrucciones necesarias para la programación de dicho código fuente en los Kilobots. Los pasos que han de seguirse para llevar a cabo este proceso son los siguientes:
\begin{itemize}
\item Situarse en el directorio de kilolib-master
\begin{listing}[style=consola, numbers=none]
$     cd kilolib-master
\end{listing}
\item Utilizar el archivo makefile para compilar el archivo blank.c que es el archivo que contiene el codigo fuente
\begin{listing}[style=consola, numbers=none]
$     make blank
\end{listing}
\end{itemize}
\item \textbf{Selección en la interfaz de Kilogui del programa a ejecutar:} En este paso, abriremos la interfaz gráfica de Kilogui y procederemos a cargar el archivo .hex generado del paso anterior en los Kilobots. Para ello:
\begin{itemize}
\item Abrimos la interfaz de Kilogui con privilegios de superusuario, al ser esta la única forma de permitir la conexión FTDI.
\begin{listing}[style=consola, numbers=none]
$     sudo kilogui
\end{listing}
\item Haciendo click en el botón select file elegimos el archivo blank.hex generado en el subdirectorio build de kilolib-master. 
\end{itemize}
\item \textbf{Carga y ejecución del programa en la arquitectura:} Una vez que en el paso anteiror se ha seleccionado el archivo que se quiere ejecutar, procederemos a cargar este programa en los Kilobots y a ejecutarlo, siguiendo para ello los siguientes pasos, que se llevaŕan a cabo desde la interfaz de Kilogui.
\begin{itemize}
\item \textbf{Bootload:} El primer paso una vez que se ha seleccionado el archivo, es accionar el botón Bootload, que preparará e iniciará el sistema de arranque de los Kilobots, que estarán a la espera de recibir un programa. Al pulsar este botón, los Kilobots pondrán su LED en azul. 
\item \textbf{Upload:} Una vez que ya se ha activado el LED con la luz azul que indica que el proceso de arranque de los Kilobots se ha completado con éxito, pulsamos el botón de upload, que cargará el programa seleccionado en el paso anterior. Durante el proceso de carga, que puede durar unos segundos, la luz del LED de los kilobots parpadeará de color amarillo, y una vez que la carga se haya completado la luz del LED parpadeará con menor intensidad, del mismo modo que cuando el Kilobot se encuentra en modo pausa. 
\item \textbf{Run:} Cuando el programa ha sido cargado, al accionar el botón de Run comenzará la ejecución del mismo hasta que el usuario accione el botón de Pause, que pondrá a los Kilobots en modo de pausa, el botón Sleep, que los dormirá, diferenciándose del botón de pausa en que en el modo Sleep el consumo de energía es mucho menor, o el botón de reset, que reiniciará el sistema de los Kilobots. 
\end{itemize}
\end{enumerate}
