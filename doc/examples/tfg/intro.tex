\chapter{Introducción}
\label{chap:Introduccion}

\drop{D}{esde} tiempos pretéritos, la observación de la naturaleza ha propiciado la mayor parte de los cambios evolutivos en la vida del hombre y ha sido la piedra angular sobre la que se han apoyado la mayoría de los avances científicos y tecnológicos producidos hasta la fecha. 

Por esto, se considera a la observación de la naturaleza la llave maestra del método científico, que parte de la contemplación y el análisis del medio que nos rodea, para cuestionarnos el por qué de los cambios de nuestro mundo, con el objetivo de llegar a una explicación racional, exacta y demostrable de cada uno de los fenómenos que lo componen y que  rodean nuestro entorno. 

Las ciencias de la computación, y más concretamente la inteligencia artificial, que nace en la década de los cincuenta, trata de emular el comportamiento de los seres humanos y su modo de razonamiento, para construir sistemas capaces de  desarrollar tareas que requieren de inteligencia. Para ello, esta disciplina toma como modelo de referencia el modelo de razonamiento humano y su forma de proceder. De este modo, para obrar del mismo que el ser humano, nuestros sistemas deberán ser dotados de una capacidad de observación del medio que los rodea, para detectar así su entorno y actuar de manera consecuente e inteligente. 

Por otra parte, la observación de la organización y del comportamiento de grandes cantidades de seres vivos, es tomada como fuente de inspiración para llevar a cabo multitud de sistemas formados por diversas entidades que han de tomar decisiones y llegar a acuerdos y consenso entre sus intereses. Así, la observación de la naturaleza y los mecanismos naturales hacen de estos la base de sistemas robóticos colectivos y de sistemas que, a su vez, están formados por subsistemas que disponen de estos mecanismos, como son, por ejemplo, muchos de los electrodomésticos que utilizamos a diario. 

El presente proyecto, se enmarca en el campo de la computación evolutiva, y dentro de ella, en el de los algoritmos bioinspirados, centrándose en el control de formaciones de múltiples nanorobots móviles, utilizando una arquitectura hardware concreta. Este estudio detallado permitirá la implementación de mecanismos y comportamientos biológicos, así como de conductas de locomoción inteligentes.

Así, este trabajo supone una toma de contacto en este campo de investigación tan amplio y con una aplicación práctica relevante y trascendental en la vida diaria de los seres humanos, mejorando la funcionalidad de muchos sistemas que utilizamos en nuestro día a día y posibilitando de una forma cada vez más sofisticada, ingenieril y científica, la interconexión e integración de los dispositivos que utilizamos en nuestra vida diaria.   




\section{Inteligencia Artificial}

Dentro de las ciencias de la computación, el proyecto realizado se enmarca en el ámbito de investigación de la inteligencia artificial (\acs{IA})~\cite{Inteligencia}, y más concretamente de la robótica. A pesar de ser la inteligencia artificial uno de los campos de las ciencias de la computación más prolífico y prometedor, es difícil dar una definición universal de este campo de estudio, ya que son muchos los matices, los enfoques y los énfasis, que los investigadores han propuesto a lo largo de la historia. \\
Entre la multitud de definiciones y clasificaciones dadas a lo largo del tiempo, destacan, por una parte, aquellas que se refieren a procesos mentales y razonamiento y, por otra parte, aquellas que se refieren a la conducta, también denominadas, definiciones conductistas. Sin embargo, también predominan otros enfoques, donde se mide el objetivo de la inteligencia artificial en base a la similitud con la forma de actuar del ser humano, mientras que otro enfoque propone la racionalidad como meta y objetivo principal de este tipo de sistemas, postulando que un sistema racional, será aquel que ``haga lo correcto'' en función de sus conocimientos (ver cuadro \ref{tab:enfoquesIA}). \\
Así pues, podemos enumerar cuatro enfoques distintos, dentro de los que distinguiremos aquellas definiciones más trascendentales: (ver cuadro 1.2).\\

\begin{table}[h]\label{tab:enfoquesIA}
\centering
\begin{tabular}{@{}lllll@{}}
\toprule
 & Imitación del Humano & Concepto de Racionalidad &  \\ \midrule
 Pensamiento& Piensan como humanos&  Piensan racionalmente& \\
 Actuación& Actúan como humanos&  Actúan Racionalmente& \\ \bottomrule
\end{tabular}
\caption{Enfoques históricos de la Inteligencia Artificial}
\end{table}

Estas definiciones, aportan luz y acotan el ámbito de estudio y de aplicación de nuestro trabajo, pues nuestros robots deberán ser entidades, que de algún modo, piensen como humanos y resuelvan problemas que hasta el momento, requieren de inteligencia. \\
En este proyecto, la inteligencia artificial goza de un papel trascendental, pues toda la funcionalidad desarrollada está basada en el procesamiento inteligente de datos e información, como son los mensajes que los robots envían en cada momento determinado, la información del entorno de la que se dispone, el comportamiento autónomo e inteligente, que se puede observar a la hora de establecer una formación o el reconocimiento de patrones, entre otros.

\input{tables/IA.tex} 

\section{Teoría de Agentes}

La teoría de agentes \cite{Agents} es otro campo perteneciente a la inteligencia artificial y que se centra en el estudio de los agentes inteligentes. Dos de las definiciones más extendidas de agente inteligente son las que se muestran a continuación:\\
\textbf{Definición 1.2.1} (Agente Inteligente). Un agente es cualquier cosa capaz de percibir su medioambiente con la ayuda de sensores y actuar en ese medio utilizando actuadores.\\
\textbf{Definición 1.2.2} (Agente Inteligente II). Un agente es un sistema de computación localizado en un entorno, que es capaz de percibir, y sobre el que actúa, modificándolo, para alcanzar los objetivos para los que fue diseñado.

Del mismo modo que los seres humanos tenemos un conjunto de órganos receptores, que se encargan de captar los estímulos de nuestro entorno, y unos órganos efectores, que ejecutan las acciones que el cerebro les ha emitido, un agente inteligente tendrá de forma análoga unos receptores, que, generalmente, son sensores que permiten captar los cambios en el entorno, denominados percepciones, y unos efectores, que realizarán la acción adecuada en cada momento. La figura \ref{fig:agente} representa la estructura básica general de un agente inteligente. Una acción tiene una serie de precondiciones que deben de cumplirse para que esta pueda llevarse a cabo y una serie de postcondiciones, o efectos que se producen en el entorno al realizar una determinada acción. El agente seleccionará de las diferentes acciones a realizar aquella que maximice la medida de rendimiento del agente. La medida de rendimiento es una función que para un estado concreto, indica cuál será el rendimiento del agente al llevar a cabo una acción determinada. Como se ha dicho anteriormente, una vez que el agente haya seleccionado la acción a realizar, ésta será ejecutada por los efectores. Ejemplos de los efectores en un agente robótico pueden ser: la rotación de un motor, apertura o cierre de articulaciones o válvulas, movimiento de articulaciones, sistemas de generación de voz etc. \\
Aparte de estas características mencionadas anteriormente, un agente debe ser una entidad autónoma, capaz de, mediante sus propios algoritmos o módulos de aprendizaje, llevar a cabo las acciones para las que ha sido diseñado. En este ámbito, un agente deberá ser capaz de comunicarse con sus congéneres y de negociar con otros agentes, de manera que represente nuestros intereses de la mejor manera posible.

La aparición de los agentes inteligentes y la autonomía, iniciativa y proactividad que se espera de ellos, conduce al nacimiento de los sistemas multiagentes (\acs{SMA})  y las sociedades de agentes. En este tipo de sistemas, nos encontramos con un amplio conjunto de agentes inteligentes, cada uno con unos intereses y objetivos particulares, que tendrán que negociar con otros agentes, o bien un conjunto de agentes con un objetivo común, y que tendrán que supeditar sus intereses particulares al objetivo común del sistema completo. 
La teoría de agentes, juega un papel crucial en este proyecto, ya que el control de formaciones supone la comunicación, consenso y acuerdo de diferentes agentes. Por otra parte, se puede aplicar la definición de agente proporcionada al principio de la sección a los Kilobots, comprobando que éstos son entidades capaces de captar percepciones en su entorno a través de sensores y en base a esas percepciones, ejecutar acciones asociadas a dichas percepciones, como, por ejemplo, cambio de movimiento, envío de un mensaje... Por otra parte, estamos sin duda ante un sistema de múltiples agentes inteligentes, donde la autonomía, la comunicación, acuerdo y negociación serán aspectos fundamentales a la hora de establecer una formación y conmutar de una formación a otra.


\begin{figure}
  \centering
    \includegraphics[width=0.8\textwidth,height=0.25\textheight]{Figuras/Agentes}
  \caption{Ejemplo de Agente Inteligente}
  \label{fig:agente}
\end{figure}

\section{Robótica}

El otro campo de conocimiento fundamental para el desarrollo del trabajo es el de la robótica. La robótica es una rama de la tecnología, algunos científicos la consideran una ciencia, que estudia el diseño y la construcción de máquinas cuyo objetivo es desempeñar tareas que realiza el ser humano y para las cuales se requiere de inteligencia. Podemos decir que la robótica bebe de otras ciencias y tecnologías como son: los autómatas programables, las máquinas de estados, la informática o la mecánica. \\
No obstante, si queremos dar una definición formal de robótica \cite{robots}, podríamos hacerlo de la siguiente forma:\\
\textbf{Definición 1.3.1 }(Robótica).El conjunto de conocimientos teóricos y prácticos que permiten concebir, realizar y automatizar sistemas basados en estructuras mecánicas poli articuladas, dotados de un determinado grado de inteligencia.\\
Por otra parte, y para comprender mejor este campo, es interesante reflexionar sobre la definición de sistema robótico, o al menos apuntarla. \\
\textbf{Definición 1.3.2 }(Sistema Robótico).Un sistema robótico es aquel capaz de recibir información, comprender su entorno a través del empleo de modelos, de formular y ejecutar planes así como de controlar o supervisar su operación\\
Si tenemos que destacar a alguno de los muchos investigadores sobre robótica a lo largo de la historia, no podemos dejar de nombrar a Isaac Asimov, padre de la robótica por antonomasia, ya que nadie ha definido tan minuciosamente las actitudes y posibilidades de estas máquinas como lo ha hecho él, postulándolas en sus \emph{Tres reglas fundamentales de la robótica}, a saber: 
\begin{enumerate}
\item Ningún robot puede hacer daño a un ser humano, o permitir que se le haga daño por no actuar.
\item Un robot debe obedecer las órdenes dadas por un ser humano, excepto si éstas órdenes entran en conflicto con la primera ley.
\item Un robot debe proteger su propia existencia en la medida en que ésta protección no sea incompatible con las leyes anteriores. 
\end{enumerate}
Antes de pasar a dar una definición más formal de robot, es conveniente dejar a un lado los prejuicios y la creencia general formada en la sociedad acerca de este término, que, basándose muchas veces en películas de ciencia-ficción, creen que los robots son malvados por naturaleza. Ante todo, debemos saber que los robots son lo que los hombres quieran que lleguen a ser. \\
Dar una definición de robot no es nada sencillo, podríamos compararlo con definir algo como el aburrimiento o la felicidad; sabemos si estamos aburridos o somos felices, pero es difícil de explicar con palabras.\\
Podemos decir que un robot es un dispositivo, generalmente mecánico, que desempeña un conjunto de tareas automáticamente, ya esté supervisado por un humano, a través de un programa predefinido o siguiendo un conjunto de reglas generales, utilizando técnicas de inteligencia artificial. Generalmente, de acuerdo a las definiciones o enfoques de inteligencia artificial que hemos dado previamente, estas tareas suelen ser tareas que, realizadas por un ser humano, requieren de inteligencia. \\
Por otra parte,también se puede definir un robot como una entidad hecha por el hombre con un cuerpo y una conexión de retroalimentación inteligente entre el sentido y la acción. Normalmente, esta inteligencia viene dada por una computadora o un microcontrolador que ejecuta un programa. No obstante, existen multitud de robots con inteligencia alámbrica. \\
Pero, al margen de estos enfoques, siempre susceptibles a la subjetividad del autor que los emite, podemos dar una definición emitida por un organismo especialista en robótica, como es la \acs{RIA} (Robot Industries Associaton), lo que nos permite tener una definición más genérica y válida, aceptada por una comunidad. \\
\textbf{Definición 1.3.3 }(Robot).Un robot es un manipulador reprogramable y multifuncional, diseñado para mover y cargar piezas, herramientas o dispositivos especiales, según trayectorias variadas y programadas.\\
Sin embargo, teniendo unas mínimas nociones de robótica, podemos ver que ésta definición no abarca a todos los robots, pues da un sentido al término puramente manipulador, mientras que tenemos gran cantidad de robots, no solo manipuladores, ni hardware, sino que también existen robots software, como por ejemplo, agentes software, luego, a pesar del formalismo, que la definición sirva para centrar el término y obtener una definición genérica, pero que no cierre puertas al lector para abarcar el significado completo del término, pues la historia y los avances en estos campos, han ido ampliando las fronteras del mismo e incluso hoy, éstas siguen aumentando.\\
Si bien, a pesar de dar varias definiciones del término robot, no hemos visto ningún ejemplo de qué es un robot y qué no lo es, por eso, la siguiente definición de autómata programable, nos ayudará a tratar de ponerle "puertas al campo".\\
\textbf{Definición 1.3.4 }(Autómata programable). Es cualquier máquina electrónica que se ha diseñado para controlar en tiempo real un proceso.\\ 
Esta máquina carece de inteligencia como tal y reacciona exactamente igual ante sucesos iguales. Con todo ello, es frecuente que un autómata forme parte de un sistema robótico, controlando las señales del proceso y dirigiendo actuaciones. Con esta definición, podemos llegar a la conclusión de que los conceptos de robot y autómata programable aparecerán ligados, si bien un robot no consistirá únicamente en un autómata, pero sí tendrá como trasfondo un autómata programable, que debidamente modificado, da como resultado alguna de las definiciones de robot escritas anteriormente.\\
A continuación, vamos a dar dos posibles clasificaciones \cite{clasr} o taxonomías de robots, siendo la primera la más utilizada, basada en la evolución histórico-científica y una segunda clasificación, cimentada en la arquitectura de los robots.
\begin{enumerate}
 \item \textbf{Clasificación Histórica}
       \begin{enumerate}
           \item \textbf{Primera Generación. Manipuladores:} Son sistemas mecánicos multifuncionales con un sencillo sistema de control, bien manual, de secuencia fija o de secuencia variable. Un ejemplo de ellos puede verse en la Figura \ref{Manipulador}.
           \begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_1generacion}
  \caption{Robot Manipulador de Primera Generación}
  \label{Manipulador}
\end{figure}

           \item \textbf{Segunda Generación. Robots de aprendizaje:} Repiten una secuencia de movimientos que ha sido ejecutada antes por un operador humano. Esto se consigue a través de un dispositivo mecánico. El operador realiza los movimientos mientras el robot los memoriza. La Figura \ref{RobotAprendizaje} muestra un ejemplo de ellos. 
           
 \begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_2generacion}
  \caption{Robot de Aprendizaje de Segunda Generación}
  \label{RobotAprendizaje}
\end{figure}           
           
           \item \textbf{Tercera Generación. Robots con control sensorizado:} El controlador es una computadora que ejecuta las órdenes de un programa y las envía al manipulador para que realice los movimientos necesarios. Un ejemplo claro puede verse en la Figura \ref{RobotTercera}.

 \begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_3generacion}
  \caption{Robot con control sensorizado de Tercera Generación}
  \label{RobotTercera}
\end{figure}

           \item \textbf{Cuarta Generación Robots inteligentes:} Similares a los anteriores, pero incorporan sensores que envían información a la computadora de control sobre el estado del proceso, lo cual permite la toma inteligente de decisiones y el control en tiempo real del proceso. A esta categoría, pertenecen los Kilobots, representados en la Figura \ref{RobotInteligente}. 
           
            \begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/kilobot}
  \caption{Kilobot. Ejemplo de Robot Inteligente}
  \label{RobotInteligente}
\end{figure}

Un resumen de esta clasificación se muestra en la figura \ref{ClasGene}.
           
 \begin{figure}
  \centering
    \includegraphics[width=0.8\textwidth,height=0.25\textheight]{Figuras/ClasI}
  \caption{Clasificación Generacional de los Robots}
  \label{ClasGene}
\end{figure}           
           
           
           
       \end{enumerate}
   \item \textbf{Clasificación Arquitectural}
   		\begin{enumerate}
   			\item \textbf{Poliarticulados:} Todos los robots de esta categoría comparten una característica común: son robots básicamente sedentarios (aunque algunos pueden ser guiados para realizar desplazamientos limitados) y están estructurados para mover sus elementos terminales en un espacio de trabajo según un sistema de coordenadas y con un número de grados de libertad. Pertenecen a esta categoría los robots manipuladores, los robots industriales y los robots cartesianos y se suelen utilizar cuando es preciso abarcar una zona de trabajo relativamente amplia, actuar sobre objetos con un plano de simetría vertical o reducir el espacio ocupado en el suelo. Un ejemplo puede verse en la Figura \ref{Poliarticulado}.
   			
    \begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_poliarticulado}
  \caption{Robot Industrial. Ejemplo de Robot Poliarticulado}
  \label{Poliarticulado}
\end{figure}   			
   			
   			\item \textbf{Móviles:} Tienen gran capacidad de desplazamiento y están dotados de un sistema locomotor de tipo rodante, patas articuladas o algún otro tipo de mecanismo basado en carros o plataformas. Siguen su camino por un telemando o se guían por la información recibida de su entorno a través de los sensores. En sus orígenes, aseguraban el transporte de piezas de un punto a otro de una cadena de fabricación, aunque hoy en día, tienen otras utilidades, como veremos en el caso particular de los Kilobots. La Figura \ref{RobotMovil} muestra un ejemplo de robot móvil.
   			
\begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_movil}
  \caption{Ejemplo de Robot Movil}
  \label{RobotMovil}
\end{figure}  
   			
   			
   			\item \textbf{Androides:} Los androides intentan emular o reproducir de forma total o parcial el comportamiento de un ser humano. Actualmente, son robots muy poco evolucionados y sin utilidad práctica, aunque sí gozan de gran interés teórico, por lo que su uso se limita primordialmente a la investigación. Uno de los problemas más importantes de estos robots y sobre el que se está investigando en la actualidad es la locomoción bípeda y el equilibro. Un ejemplo de robot androide puede verse en la Figura \ref{Androide}.
   			

\begin{figure}
  \centering
    \includegraphics[width=0.20\textwidth,height=0.10\textheight]{Figuras/Clasificacion_Robots/robot_androide}
  \caption{Ejemplo de Robot Androide}
  \label{Androide}
\end{figure}  
   			
   			
   			 
   			\item \textbf{Zoomórficos:} Este tipo de robots podría pertenecer a la categoría de androides, sin embargo, se estudian como una categoría aparte ya que sus sistemas de locomoción imitan a los diversos seres vivos. Dentro de los robots zoomórficos podemos distinguir dos grupos claramente diferenciados: caminadores y no caminadores. Los no caminadores actualmente están muy poco evolucionados y los caminadores, que son aquellos sobre los que se ha experimentado en muchos laboratorios con el objetivo de desarrollar verdaderos vehículos terrenos o sistemas de pilotaje autónomo, tienen gran aplicación en los campos de la exploración espacial y el estudio de los volcanes. La Figura \ref{Zoomorfico} muestra un ejemplo de robot de este tipo. 
   			

\begin{figure}
  \centering
    \includegraphics[width=0.25\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_zoomorfico}
  \caption{Ejemplo de Robot Zoomórfico}
  \label{Zoomorfico}
\end{figure}  
   			   			
   			
   			
   			\item \textbf{Híbridos:} Son aquellos robots que no pertenecen a ninguna de las categorías anteriores, o bien están formados por la conjunción o yuxtaposición de algunas de las categorías expuestas. Un ejemplo podría ser un robot formado por la yuxtaposición de un cuerpo formado por un carro móvil y de un brazo semejante al brazo de un robot industrial o los robots antropomorfos. Un ejemplo de robot híbrido puede apreciarse en la Figura \ref{Hibrido}.
   			

\begin{figure}
  \centering
    \includegraphics[width=0.15\textwidth,height=0.15\textheight]{Figuras/Clasificacion_Robots/robot_hibrido}
  \caption{Ejemplo de Robot Híbrido}
  \label{Hibrido}
\end{figure}  
   			   			
   			
   			
   		\end{enumerate}
\end{enumerate} 
Un resumen de esta clasficación arquitectural, puede verse en la figura \ref{ClasArqui} . 

 \begin{figure}
  \centering
    \includegraphics[width=0.9\textwidth,height=0.25\textheight]{Figuras/ClasII}
  \caption{Clasificación Arquitectural de los Robots}
  \label{ClasArqui}
\end{figure}           
           




\section{Estructura del documento}

El resto del documento, que desarrolla la construcción del presente proyecto, se estructura de la siguiente forma:

\begin{definitionlist}
\item[Capítulo \ref{chap:objetivos}: \nameref{chap:objetivos}] Donde se definen y explicitan los objetivos que este proyecto pretende alcanzar así como la finalidad del mismo.
\item[Capítulo \ref{chap:antecedentes}: \nameref{chap:antecedentes}] En donde se pondrá en contexto el trabajo realizado en este proyecto.
\item[Capítulo \ref{chap:metodo}: \nameref{chap:metodo}] Que desarrolla la metodología de desarrollo empleada durante la realización del proyecto.
\item[Capítulo \ref{chap:propuesta}: \nameref{chap:propuesta}] Donde se define con todo detalle la propuesta desarrollada en este proyecto.
\item[Capítulo \ref{chap:resultados}: \nameref{chap:resultados}] Capítulo que resalta la implementación de la propuesta en la arquitectura Kilobots, así como los resultados obtenidos tras la implantación de la propuesta en la arquitectura. 
\item[Capítulo \ref{chap:conclusiones}: \nameref{chap:conclusiones}] En este último capítulo se aportan las conclusiones extraídas de la realización del proyecto así como una serie de propuestas de cara al futuro.
\end{definitionlist}



% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
