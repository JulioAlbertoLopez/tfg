\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {vii}}{chapter*.2}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.3}
\contentsline {chapter}{\'Indice general}{\es@scroman {xiii}}{chapter*.4}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xvii}}{chapter*.5}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xix}}{chapter*.6}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xxiii}}{chapter*.7}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xxv}}{chapter*.8}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Inteligencia Artificial}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Teor\IeC {\'\i }a de Agentes}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Rob\IeC {\'o}tica}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Estructura del documento}{10}{section.1.4}
\contentsline {chapter}{\numberline {2}Objetivos}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivo general}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Objetivos espec\IeC {\'\i }ficos}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Objetivo 1: Disponer de un entorno de desarrollo}{13}{subsection.2.2.1}
\contentsline {subsubsection}{Entorno Software}{13}{subsection.2.2.1}
\contentsline {subsubsection}{Entorno Hardware}{14}{subsection.2.2.1}
\contentsline {subsubsection}{Toma de decisiones para el entrenamiento}{15}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Objetivo 2: Constituci\IeC {\'o}n de la configuraci\IeC {\'o}n y la estructura de la formaci\IeC {\'o}n}{15}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Objetivo 3: Actividad coordinada dentro de la formaci\IeC {\'o}n}{16}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Herramientas y medios de trabajo}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Medios Software}{16}{subsection.2.3.1}
\contentsline {subsubsection}{Lenguajes de Programaci\IeC {\'o}n y Herramientas de Implementaci\IeC {\'o}n}{16}{subsection.2.3.1}
\contentsline {subsubsection}{Gesti\IeC {\'o}n de la documentaci\IeC {\'o}n}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Medios Hardware}{17}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Antecedentes}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Computaci\IeC {\'o}n Evolutiva}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Fundamentos de Computaci\IeC {\'o}n Evolutiva}{20}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}La evoluci\IeC {\'o}n de Darwin}{21}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Gen\IeC {\'e}tica Molecular}{23}{subsection.3.1.3}
\contentsline {subsubsection}{El ADN}{25}{figure.3.6}
\contentsline {subsection}{\numberline {3.1.4}Historia de la computaci\IeC {\'o}n Evolutiva}{27}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Aplicaciones de la Computaci\IeC {\'o}n Evolutiva}{28}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Algoritmos Bioinspirados}{31}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Concepto y definici\IeC {\'o}n}{31}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Elementos de un Algoritmo Bioinspirado}{33}{subsection.3.2.2}
\contentsline {subsubsection}{Inicializaci\IeC {\'o}n}{33}{figure.3.11}
\contentsline {subsubsection}{Poblaci\IeC {\'o}n}{33}{figure.3.11}
\contentsline {subsubsection}{Representaci\IeC {\'o}n}{34}{figure.3.11}
\contentsline {subsubsection}{Funci\IeC {\'o}n de Evaluaci\IeC {\'o}n o Fitness}{35}{figure.3.11}
\contentsline {subsubsection}{Mecanismos de selecci\IeC {\'o}n o supervivencia del m\IeC {\'a}s fuerte}{35}{figure.3.11}
\contentsline {subsubsection}{Operadores de Variaci\IeC {\'o}n}{35}{figure.3.11}
\contentsline {subsubsection}{Reemplazo o eliminaci\IeC {\'o}n del m\IeC {\'a}s d\IeC {\'e}bil}{36}{figure.3.11}
\contentsline {subsubsection}{Condici\IeC {\'o}n de Terminaci\IeC {\'o}n}{36}{figure.3.11}
\contentsline {subsection}{\numberline {3.2.3}Ejemplos Pr\IeC {\'a}cticos}{37}{subsection.3.2.3}
\contentsline {subsubsection}{El problema de las n-reinas}{37}{subsection.3.2.3}
\contentsline {subsubsection}{El problema de la mochila}{39}{figure.3.12}
\contentsline {subsubsection}{El problema del viajante de comercio}{42}{equation.3.2.2}
\contentsline {subsection}{\numberline {3.2.4}Rendimiento y Eficiencia}{43}{subsection.3.2.4}
\contentsline {subsubsection}{Optimizaci\IeC {\'o}n global en algoritmos bioinspirados}{44}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Swarm Intelligence}{45}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Ant Colony Optimization (ACO)}{46}{subsection.3.3.1}
\contentsline {subsubsection}{El experimento del puente}{47}{subsection.3.3.1}
\contentsline {subsubsection}{De la hormiga natural a la hormiga artificial}{48}{figure.3.15}
\contentsline {subsubsection}{Simplest Ant Colony Optimization S-ACO}{49}{equation.3.3.9}
\contentsline {subsection}{\numberline {3.3.2}La metaheur\IeC {\'\i }stica de ACO}{50}{subsection.3.3.2}
\contentsline {subsubsection}{El algoritmo}{53}{Item.51}
\contentsline {section}{\numberline {3.4}Rob\IeC {\'o}tica M\IeC {\'o}vil}{54}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Sistemas multi-robot cooperativos}{56}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Planificaci\IeC {\'o}n de Movimientos }{57}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}M\IeC {\'e}todo}{59}{chapter.4}
\contentsline {section}{\numberline {4.1}Elecci\IeC {\'o}n de la metodolog\IeC {\'\i }a de desarrollo}{59}{section.4.1}
\contentsline {section}{\numberline {4.2}Extreme Programming (XP)}{59}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}El principal problema}{60}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Las cuatro variables}{62}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Los cuatro valores}{62}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Las cuatro tareas}{63}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Aplicaci\IeC {\'o}n de la metodolog\IeC {\'\i }a}{65}{section.4.3}
\contentsline {chapter}{\numberline {5}La Propuesta}{73}{chapter.5}
\contentsline {section}{\numberline {5.1}Enfoques}{73}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Enfoque de conocimiento global}{73}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Local Sensing}{74}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Compendio de aportaciones}{75}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Algoritmos de Consenso en Sistemas Multiagentes}{75}{subsection.5.2.1}
\contentsline {subsubsection}{Algoritmo de Paxos}{76}{subsection.5.2.1}
\contentsline {subsubsection}{Algoritmo gen\IeC {\'e}rico de Fredslund y Matari\IeC {\'c}}{76}{subsection.5.2.1}
\contentsline {subsubsection}{Control de formaciones basado en comportamientos}{77}{figure.5.3}
\contentsline {subsubsection}{Algoritmos en enjambres de hormigas}{79}{figure.5.5}
\contentsline {subsubsection}{Otros trabajos}{80}{figure.5.6}
\contentsline {subsection}{\numberline {5.2.2}Algoritmos de Seleccion del L\IeC {\'\i }der}{81}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Algoritmos de Establecimiento y Control de formaciones}{82}{subsection.5.2.3}
\contentsline {subsubsection}{Leyes de Retroalimentaci\IeC {\'o}n}{82}{subsection.5.2.3}
\contentsline {subsubsection}{Otros trabajos}{84}{Item.85}
\contentsline {section}{\numberline {5.3}Definici\IeC {\'o}n del problema}{86}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}El algoritmo}{87}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Implementaci\IeC {\'o}n de la Propuesta}{92}{subsection.5.3.2}
\contentsline {subsubsection}{Implementaci\IeC {\'o}n del Simulador}{95}{Item.96}
\contentsline {paragraph}{Estructuras de datos}{95}{Item.96}
\contentsline {paragraph}{Inicio. Representaci\IeC {\'o}n.}{96}{figure.5.13}
\contentsline {paragraph}{Descubrimiento}{97}{figure.5.14}
\contentsline {paragraph}{Selecci\IeC {\'o}n de la Configuraci\IeC {\'o}n}{97}{equation.5.3.12}
\contentsline {paragraph}{Consenso}{98}{figure.5.15}
\contentsline {paragraph}{Selecci\IeC {\'o}n del L\IeC {\'\i }der}{99}{figure.5.15}
\contentsline {chapter}{\numberline {6}Resultados}{101}{chapter.6}
\contentsline {section}{\numberline {6.1}Implementaci\IeC {\'o}n de la propuesta en Kilobots}{101}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}L\IeC {\'\i }nea base}{101}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Inicializacion}{105}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Descubrimiento}{110}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Selecci\IeC {\'o}n de la Configuraci\IeC {\'o}n}{113}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Consenso}{116}{subsection.6.1.5}
\contentsline {subsection}{\numberline {6.1.6}Lucernario}{119}{subsection.6.1.6}
\contentsline {subsection}{\numberline {6.1.7}Selecci\IeC {\'o}n del l\IeC {\'\i }der}{122}{subsection.6.1.7}
\contentsline {chapter}{\numberline {7}Conclusiones y Trabajos futuros}{125}{chapter.7}
\contentsline {section}{\numberline {7.1}Conclusiones}{125}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Objetivos cumplidos}{126}{subsection.7.1.1}
\contentsline {section}{\numberline {7.2}Trabajos Futuros}{127}{section.7.2}
\contentsline {chapter}{\numberline {A}Configuraci\IeC {\'o}n del Entorno}{131}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Configuraci\IeC {\'o}n del entorno software}{131}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Configuraci\IeC {\'o}n del entorno hardware}{133}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Configuraci\IeC {\'o}n del entorno de ejecuci\IeC {\'o}n}{141}{section.Alph1.3}
\contentsline {chapter}{\numberline {B}API Kilobots}{145}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}Ficheros de Cabecera}{145}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}Estructuras de datos}{145}{section.Alph2.2}
\contentsline {section}{\numberline {B.3}Variables}{146}{section.Alph2.3}
\contentsline {section}{\numberline {B.4}Funciones}{147}{section.Alph2.4}
\contentsline {section}{\numberline {B.5}Macros}{148}{section.Alph2.5}
\contentsline {section}{\numberline {B.6}Ejemplos}{149}{section.Alph2.6}
\contentsline {subsection}{\numberline {B.6.1}Parpadeo}{150}{subsection.Alph2.6.1}
\contentsline {subsection}{\numberline {B.6.2}Movimiento}{151}{subsection.Alph2.6.2}
\contentsline {subsection}{\numberline {B.6.3}Comunicacion}{152}{subsection.Alph2.6.3}
\contentsline {chapter}{Referencias}{155}{appendix*.9}
