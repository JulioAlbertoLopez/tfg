/* Funcion estable de Descubrimiento*/

void Discover(){
    uint8_t var_aleatoria, num_iteraciones;
    num_iteraciones = 0;
    while(num_iteraciones <10){
    	printf(".-.-.-.-.-.-.-.-.\n");
    	printf("Iteracion numero %d \n", num_iteraciones);
    if (state == 1){
        var_aleatoria = aleatorio;
        /*printf("He elegido el aleatorio\n");*/
    }
    else if(state == 2){
        var_aleatoria = aleatorio_rediscover;
        /*printf("He elegido el aleatorio rediscover\n");*/
    }
    printf("Aleatorio vale %d\n",var_aleatoria);
    aleatorio_alternancia = rand_soft();
    printf("Aleatorio de alternancia vale %d\n", aleatorio_alternancia);
    if(var_aleatoria > 128  && aleatorio_alternancia > 128){
    	delay(rand_soft());	
        if (message_sent) {
            message_sent = 0;
            printf("Mensaje enviado con exito\n");
            set_color(RGB(1,0,1));
        }
        var_aleatoria = 0;   
    }
    else{
        set_color(RGB(0,0,0));
        printf("Esperando a recibir un mensaje.....\n");
        if (new_message) {
            dist = estimate_distance(&dist_measure);
           /* printf("La distancia es: %d\n", dist);*/
            printf("Mensaje recibido con exito\n");
            Elemento e = {data,dist};
            push(&Vecinos,e);
            Lista_Vecinos[Vecinos.num_elem] = data;
            printf("El numero de elementos de Vecinos es %d \n",Vecinos.num_elem);
            set_color(RGB(1,1,0));
            delay(100);
            set_color(RGB(0,0,0));
            new_message = 0;
        }
        if(var_aleatoria > 128){
        	var_aleatoria = 0;
        }
        else{
        	var_aleatoria = (uint8_t) 130;
    	}
    }
    printf("Aleatorio vale %d\n",var_aleatoria);
    if (state == 1){
        aleatorio = var_aleatoria;
        /*printf("He modificado el aleatorio\n");*/
    }
    else if(state == 2){
        aleatorio_rediscover = var_aleatoria;
        /*printf("He modificado el aletorio rediscover\n");*/
    }
    num_iteraciones = num_iteraciones +1;
    printf(".-.-.-.-.-.-.--.-.\n");
}
}