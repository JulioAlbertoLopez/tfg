#include "kilolib.h"

uint8_t aleatorio, semilla;

void setup() {
    /*Codigo de Inicializacion, que unicamente se ejecutara una vez*/
    semilla = rand_hard();
    rand_seed(semilla);
}

void loop() {
    /*Codigo que se ejecuta repetidamente*/
    aleatorio = rand_soft();
    if(aleatorio <= 80){
        set_color(RGB(1,0,0));
        spinup_motors();
        set_motors(kilo_straight_left, kilo_straight_right);
    }
    else if(aleatorio <= 160){
        set_color(RGB(0,1,0));
        spinup_motors();
        set_motors(kilo_straight_left, 0); 
    }
    else{
        set_color(RGB(0,0,1));
        spinup_motors();
        set_motors(0, kilo_straight_right);
    }
int main() {
    /*Inicializacion del Hardware*/
    kilo_init();
    /*Registro del programa*/
    kilo_start(setup, loop);

    return 0;
}