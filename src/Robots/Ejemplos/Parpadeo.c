#include "kilolib.h"

uint8_t estado = 3;

void setup() {
    /*Codigo de Inicializacion, que unicamente se ejecutara una vez*/
}

void loop() {
    /*Codigo que se ejecuta repetidamente*/
    if(estado % 3 == 0)
        set_color(RGB(0,0,1));
    else if(estado %3 == 1)
        set_color(RGB(1,0,0));  
    else
        set_color(RGB(0,1,0));
    delay(1000);
    estado = estado +1
}

int main() {
    /*Inicializacion del Hardware*/
    kilo_init();
    /*Registro del programa*/
    kilo_start(setup, loop);

    return 0;
}