#include <stdio.h>
typedef struct{
	int ref; /*Identificador del elemento*/
	int key; /*Clave por la que ordenar el elemento*/
} Elemento;

int get_ref(Elemento e);
double get_key(Elemento e);
void set_ref(Elemento e, int n_ref);
void set_key(Elemento e, double n_key);
void imprimir_elemento(Elemento e);

int get_ref(Elemento e){
	return e.ref;
}

double get_key(Elemento e){
	return e.key;
}

void set_ref(Elemento e, int n_ref){
	e.ref = n_ref;
}

void set_key(Elemento e, double n_key){
	e.key = n_key;
}

void imprimir_elemento(Elemento e){
	printf("Elemento --> Ref: %d Clave: %f\n",get_ref(e),get_key(e));
}