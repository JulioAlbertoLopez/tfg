void Seleccion_Configuracion(){
    int aleatorio_movimiento, i, j, k, num_iter, tam, rellenar_en, contador_propuestas;
    num_iter = 0;
while(num_iter < 10){
	printf("-.-.-.-.-.-.-.\n");
	printf("Iteracion numero %d\n", num_iter);
    printf("El numero de vecinos es %d \n",Vecinos.num_elem);
    if(Vecinos.num_elem <1 ){
        aleatorio_movimiento = rand_soft();
        printf("El aleatorio de movimiento es %d\n", aleatorio_movimiento);
        if(aleatorio_movimiento < 85){
            spinup_motors();
            set_motors(kilo_turn_left, 0);
            delay(3000);
            set_motors(0,0);
            printf("Me muevo hacia la izquierda\n");
            reset_heap(&Vecinos);
            reset_vector(Lista_Vecinos);
            Discover();
            printf("He vuelto de Discover\n");
            
        }
        else if(aleatorio_movimiento < 170){
            spinup_motors();
            set_motors(0, kilo_turn_right);
            delay(3000);
            set_motors(0,0);
            printf("Me muevo hacia la derecha\n");
            reset_heap(&Vecinos);
            reset_vector(Lista_Vecinos);
            Discover();
            printf("He vuelto de Discover\n");
            
        }
        else{
            spinup_motors();
            set_motors(kilo_turn_left, kilo_turn_right);
            delay(3000);
            set_motors(0,0);
            printf("Me muevo hacia delante\n");
            reset_heap(&Vecinos);
            reset_vector(Lista_Vecinos);
            Discover();
            printf("He vuelto de Discover\n");            

        }

    }
    else{
    	printf("Tengo mas de dos vecinos\n");
    	set_motors(0,0);
    	set_color(RGB(0,0,0));
    	delay(500);
    	set_color(RGB(1,1,1));

    }
    num_iter = num_iter +1;
    printf(".-.-.-.-.-\n");
 }
    if(num_iter == 10){
    	printf("Ya llevo diez\n");
    	tam = Vecinos.num_elem +1;
    	printf("Hay %d vecinos \n", tam);
    	Elemento Vecinos_Robot [tam];
    	for(i = 0; i< tam; i++){
    		Elemento e = {-1,-1};
    		Vecinos_Robot[i] = e;
    	}
    	for(i = 0; i< tam; i++){
    		imprimir_elemento(Vecinos_Robot[i]);
    		printf("\n");
    	}
    	rellenar_en = 0;
    	while(Vecinos.num_elem != -1){
    		Elemento temporal = pop(&Vecinos);
    		printf("El temporal es \n");
    		imprimir_elemento(temporal);
    		printf("\n");
    		Vecinos_Robot[rellenar_en] = temporal;
    		rellenar_en = rellenar_en +1;
    	}
    	for(i = 0; i< tam; i++){
    		imprimir_elemento(Vecinos_Robot[i]);
    		printf("\n");
    	}  
    	contador_propuestas = 0;
    	for(i = 0; i<tam; i++){
    		for(j = 0; j< tam; j++){
    			if(j>=i && i!=j){
    				printf("i y j valen %d y %d \n",i,j );
    				printf("Los valores a introducir son %d y %d\n", get_ref(Vecinos_Robot[i]), get_ref(Vecinos_Robot[j]));
    				printf("Los valores a introducir son %d y %d\n", max(get_ref(Vecinos_Robot[i]), get_ref(Vecinos_Robot[j])), (10-max(get_ref(Vecinos_Robot[i]), get_ref(Vecinos_Robot[j]))*1));
    				Lista_Propuestas[contador_propuestas][0] = get_ref(Vecinos_Robot[i]);
    				Lista_Propuestas[contador_propuestas][1] = get_ref(Vecinos_Robot[j]);
    				Lista_Propuestas[contador_propuestas][2] = 1;
    				Lista_Propuestas[contador_propuestas][3] = max(get_ref(Vecinos_Robot[i]), get_ref(Vecinos_Robot[j]));
    				Lista_Propuestas[contador_propuestas][4] = (10-max(get_ref(Vecinos_Robot[i]), get_ref(Vecinos_Robot[j]))*1);
    				Elemento Propuesta = {contador_propuestas, Lista_Propuestas[contador_propuestas][4]};
    				push(&Propuestas,Propuesta);
    				contador_propuestas = contador_propuestas + 1;
    			}
    		}
    	}
	}
}
