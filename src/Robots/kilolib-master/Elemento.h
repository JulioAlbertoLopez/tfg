#include <stdio.h>
#include "kilolib.h"

typedef struct{
	uint16_t ref; /*Identificador del elemento*/
	uint16_t key; /*Clave por la que ordenar el elemento*/
} Elemento;

uint16_t get_ref(Elemento e);
uint16_t get_key(Elemento e);
void set_ref(Elemento e, uint16_t n_ref);
void set_key(Elemento e, uint16_t n_key);
void imprimir_elemento(Elemento e);

uint16_t get_ref(Elemento e){
	return e.ref;
}

uint16_t get_key(Elemento e){
	return e.key;
}

void set_ref(Elemento e, uint16_t n_ref){
	e.ref = n_ref;
}

void set_key(Elemento e, uint16_t n_key){
	e.key = n_key;
}

void imprimir_elemento(Elemento e){
	printf("Elemento --> Ref: %d Clave: %d\n",get_ref(e),get_key(e));
}