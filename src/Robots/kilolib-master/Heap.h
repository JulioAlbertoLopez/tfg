#include <stdio.h>
#include <stdlib.h>
#include "Elemento.h"
#define MAX_ELEMENT 9

#define IS_GREAT(v1, v2)  (v1 > v2)
#define IS_LESS(v1, v2)  (v1 < v2)
#define SWAP(r,s)  do{Elemento t=r; r=s; s=t; } while(0)

typedef struct{
	int num_elem; /*Numero de elementos del Heap*/
	int max_heap; /* Indica si sera o no un MaxHeap*/
	Elemento datos[MAX_ELEMENT]; /*Array con los elementos del Heap*/
} Heap;

Heap create_Heap(int Tipo);
void push(Heap *h, Elemento e);
Elemento pop(Heap *h); 
Elemento peak(Heap *h);
void imprimir_heap(Heap h);
void reset_heap(Heap *h);
void heapsort(Heap *heap);
void siftDown( Heap *heap, int start, int end);
void heapsortm(Heap *heap);
void siftDownm( Heap *heap, int start, int end);

Heap create_Heap(int Tipo){
	int i;
	Heap h;
	h.num_elem = -1;
	h.max_heap = Tipo;	
	for(i = 0; i< MAX_ELEMENT; i++){
		Elemento e = {-1,-1};
		h.datos[i] = e;
	}
	return h;
}	

void push(Heap *h, Elemento e){
	int i;
	for(i = 0; i< MAX_ELEMENT; i++){
		if(h->datos[i].ref == get_ref(e)){
			i = MAX_ELEMENT + 1;
			break;
		}
	}
	if(i != MAX_ELEMENT+1){
		if(h->num_elem <=8){
			h->num_elem++;
   	 		h->datos[h->num_elem] = e;
			if(h->max_heap == 0){
    			heapsort(h);
			}
			else{
				heapsortm(h);
			}
    	}
    }

}

Elemento pop(Heap *h){
	Elemento elem;
	h->num_elem--;
	Elemento nuevo = {-1,-1};
	int i, indice;
	indice = 0;
	for(i = 0; i< MAX_ELEMENT; i++){
		if (h->datos[i].key != -1){
			indice = i;
			elem = h->datos[i];
			i = MAX_ELEMENT;
		}
	}
	
	h->datos[indice] = nuevo;
	if(h->max_heap == 0){
		printf("Soy un heap normal");
		heapsort(h);
	}
	else{
		printf("Soy un maxheap");
		heapsortm(h);	
	}	
	return elem;
}


Elemento peak(Heap *h){
	return h->datos[0];
}

void imprimir_heap(Heap h){
	int i;
	for(i=0;i<MAX_ELEMENT;i++){
		imprimir_elemento(h.datos[i]);
	}
}

void reset_heap(Heap *h){
	int i;
	h->num_elem = -1;
	Elemento e = {-1,-1};
	for(i = 0; i< MAX_ELEMENT; i++){
		h->datos[i] = e;
	}
}

void heapsort(Heap *heap)
{
    int start, end;
    int count = heap->num_elem + 1;
 
    /* heapify */
    for (start = (count-2)/2; start >=0; start--) {
        siftDown( heap, start, count);
    }
 
    for (end=count-1; end > 0; end--) {
        SWAP(heap->datos[end],heap->datos[0]);
        siftDown(heap, 0, end);
    }
}
 
void siftDown( Heap *heap, int start, int end)
{
    int root = start;
 
    while ( root*2+1 < end ) {
        int child = 2*root + 1;
        if ((child + 1 < end) && IS_LESS(get_key(heap->datos[child]),get_key(heap->datos[child+1]))) {
            child += 1;
        }
        if (IS_LESS(get_key(heap->datos[root]), get_key(heap->datos[child]))) {
            SWAP( heap->datos[child], heap->datos[root] );
            root = child;
        }
        else
            return;
    }
}

void heapsortm(Heap *heap)
{
    int start, end;
    int count = heap->num_elem + 1;
 
    /* heapify */
    for (start = (count-2)/2; start >=0; start--) {
        siftDownm( heap, start, count);
    }
 
    for (end=count-1; end > 0; end--) {
        SWAP(heap->datos[end],heap->datos[0]);
        siftDownm(heap, 0, end);
    }
}
 
void siftDownm( Heap *heap, int start, int end)
{
    int root = start;
 
    while ( root*2+1 < end ) {
        int child = 2*root + 1;
        if ((child + 1 > end) && IS_GREAT(get_key(heap->datos[child]),get_key(heap->datos[child+1]))) {
            child += 1;
        }
        if (IS_GREAT(get_key(heap->datos[root]), get_key(heap->datos[child]))) {
            SWAP( heap->datos[child], heap->datos[root] );
            root = child;
        }
        else
            return;
    }
}