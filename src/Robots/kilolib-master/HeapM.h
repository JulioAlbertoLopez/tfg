#include <stdio.h>
#include <stdlib.h>
#include "Propuesta.h"
#define MAX_ELEMENT 9

#define IS_GREAT(v1, v2)  (v1 > v2)
#define IS_LESS(v1, v2)  (v1 < v2)
#define SWAP(r,s)  do{Propuesta t=r; r=s; s=t; } while(0)

typedef struct{
	int num_elem; /*Numero de Propuestas del HeapM*/
	Propuesta datos[MAX_ELEMENT]; /*Array con los Propuestas del HeapM*/
} HeapM;

HeapM create_HeapM();
void push(HeapM *h, Propuesta e);
Propuesta pop(HeapM *h); 
Propuesta peak(HeapM *h);
void imprimir_heap(HeapM h);
void reset_heap(HeapM *h);
void heapsort(HeapM *heap);
void siftDown( HeapM *heap, int start, int end);


HeapM create_HeapM(){
	int i;
	HeapM h;
	h.num_elem = -1;
	for(i = 0; i< MAX_ELEMENT; i++){
		Propuesta e = {-1,-1};
		h.datos[i] = e;
	}
	return h;
}	

void push(HeapM *h, Propuesta e){
	int i;
	for(i = 0; i< MAX_ELEMENT; i++){
		if(h->datos[i].ref == get_ref(e)){
			i = MAX_ELEMENT + 1;
			break;
		}
	}
	if(i != MAX_ELEMENT+1){
		if(h->num_elem <=8){
			h->num_elem++;
   	 		h->datos[h->num_elem] = e;
    		heapsort(h);
    	}
    }

}

Propuesta pop(HeapM *h){
	Propuesta elem;
	h->num_elem--;
	Propuesta nuevo = {-1,-1};
	int i, indice;
	indice = 0;
	for(i = 0; i< MAX_ELEMENT; i++){
		if (h->datos[i].key != -1){
			indice = i;
			elem = h->datos[i];
			i = MAX_ELEMENT;
		}
	}
	
	h->datos[indice] = nuevo;
	heapsort(h);
	return elem;
}


Propuesta peak(HeapM *h){
	return h->datos[0];
}

void imprimir_heap(HeapM h){
	int i;
	for(i=0;i<MAX_ELEMENT;i++){
		imprimir_Propuesta(h.datos[i]);
	}
}

void reset_heap(HeapM *h){
	int i;
	h->num_elem = -1;
	Propuesta e = {-1,-1};
	for(i = 0; i< MAX_ELEMENT; i++){
		h->datos[i] = e;
	}
}

void heapsort(HeapM *heap)
{
    int start, end;
    int count = heap->num_elem + 1;
 
    /* heapify */
    for (start = (count-2)/2; start >=0; start--) {
        siftDown( heap, start, count);
    }
 
    for (end=count-1; end > 0; end--) {
        SWAP(heap->datos[end],heap->datos[0]);
        siftDown(heap, 0, end);
    }
}
 
void siftDown( HeapM *heap, int start, int end)
{
    int root = start;
 
    while ( root*2+1 <end ) {
        int child = 2*root + 1;
        if ((child + 1 > end) && IS_GREAT(get_key(heap->datos[child]),get_key(heap->datos[child+1]))) {
            child += 1;
        }
        if (IS_GREAT(get_key(heap->datos[root]), get_key(heap->datos[child]))) {
            SWAP( heap->datos[child], heap->datos[root] );
            root = child;
        }
        else
            return;
    }
}