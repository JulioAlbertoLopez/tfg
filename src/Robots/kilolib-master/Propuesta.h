#include <stdio.h>
#include "kilolib.h"

typedef struct{
	uint8_t ref; /*Identificador del Propuesta*/
	uint16_t key; /*Clave por la que ordenar el Propuesta*/
} Propuesta;

uint8_t get_ref(Propuesta e);
uint16_t get_key(Propuesta e);
void set_ref(Propuesta e, uint8_t n_ref);
void set_key(Propuesta e, uint16_t n_key);
void imprimir_Propuesta(Propuesta e);

uint8_t get_ref(Propuesta e){
	return e.ref;
}

uint16_t get_key(Propuesta e){
	return e.key;
}

void set_ref(Propuesta e, uint8_t n_ref){
	e.ref = n_ref;
}

void set_key(Propuesta e, uint16_t n_key){
	e.key = n_key;
}

void imprimir_Propuesta(Propuesta e){
	printf("Propuesta --> Ref: %d Clave: %d\n",get_ref(e),get_key(e));
}