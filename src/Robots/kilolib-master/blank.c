#include "kilolib.h"
#define DEBUG
#include "debug.h"

// declare variables
uint16_t id;
uint8_t new_message = 0;
uint8_t aleatorio;
uint8_t message_sent = 0;
message_t msg;
// no setup code required
void setup() {
    id = kilo_uid;
    aleatorio = rand_soft();
    msg.type = NORMAL;
    msg.data[0] = id;
    msg.crc = message_crc(&msg);
 }

void loop() {
    // Blink yellow when on message received
    if(aleatorio >= 128){
        if (message_sent) {
            message_sent = 0;
            set_color(RGB(1,0,1));
            delay(20);
            set_color(RGB(0,0,0));
        }
    aleatorio = 0;
        }
    else{
        if (new_message) {
            new_message = 0;
            set_color(RGB(1,1,0));
            delay(100);
            set_color(RGB(0,0,0));
        }
        aleatorio = 130;

    }
}

message_t *message_tx() {
    return &msg;
}

void message_tx_success() {
    message_sent = 1;
}

// turn flag on message reception
void message_rx(message_t *m, distance_measurement_t *d) {
    new_message = 1;
}

int main() {
    // initialize hardware
    kilo_init();
    debug_init();
   // register message reception callback
    kilo_message_rx = message_rx;
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_success;
    // register your program
    kilo_start(setup, loop);
    return 0;
}

/*#include "kilolib.h"
#define DEBUG
#include "debug.h"

// declare variables
uint16_t id;
uint8_t message_sent = 0;
message_t msg;

void setup() {
    // initialize message
    id = kilo_uid;
    msg.type = NORMAL;
    msg.data[0] = id;
    msg.crc = message_crc(&msg);
}

void loop() {
    // blink magenta when is message sent
    if (message_sent) {
        message_sent = 0;
        set_color(RGB(1,0,1));
        delay(20);
        set_color(RGB(0,0,0));
    }
}

int main() {
    // initialize hardware
    kilo_init();
    debug_init();
    // register message_tx function
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_success;
    // register your program
    kilo_start(setup, loop);
    return 0;
}*/