#include "kilolib.h"
#include "Heap.h"
#define DEBUG
#include "debug.h"

//Constantes
#define INIT_AMARILLO 0
#define DISCOVER_ROJO 1
#define SELECT_CONFIGURATION_BLANCO 2
#define CONSENSO_AZUL 3
#define LUCERNARIO_VERDE 4
#define LIDER 5 
#define ENVIAR 0
#define K 200

//Variables
uint8_t aleatorio;
uint8_t aleatorio_alternancia;
uint8_t aleatorio_rediscover;
uint8_t new_message = 0;
uint8_t numero_iteraciones = 0;
uint8_t state = INIT_AMARILLO;
uint8_t act_state = 1;
uint8_t message_sent = 0;
uint8_t data [9] = {0,0,0,0,0,0,0,0,0};
uint8_t dist;   
uint8_t Propuesta_Elegida = 0;
uint32_t last_update;
uint16_t id;
uint16_t Lista_Vecinos [MAX_ELEMENT];
uint16_t Lista_Propuestas [10][5];
Heap Vecinos;
Heap Propuestas;
message_t msg;
distance_measurement_t dist_measure;

uint8_t change_state(volatile uint32_t kilo_ticks){
    if(kilo_ticks > last_update + 64){
        last_update = kilo_ticks;
        state = (state + 1) % 6;
        act_state = 1;
    }
    return act_state;
}

void message_rx(message_t *m, distance_measurement_t *d) {
    uint8_t i;
    for( i = 0; i<9;i++){
        data[i] = m->data[i];
    }
   /* printf("El robot que envia el mensaje es: %d", data);*/
    dist_measure = *d;
    new_message = 1;
}

message_t *message_tx() {
    return &msg;
}

void message_tx_success() {
    message_sent = 1;
}

void Inicializar_Vecinos(uint16_t Vecinos []){
    uint8_t i;
    for(i = 0; i< MAX_ELEMENT; i++){
        Vecinos[i] = -1;
    }
}

void Inicializar_Propuestas(uint16_t Propuestas [10][5]){
    int i,j;
    for(i = 0; i< 10; i++){
        for(j = 0; j< 5; j++){
            Propuestas[i][j] = -1;
        }
    }
}

void Imprimir_Vecinos(Heap Vecinos, uint16_t Vecinos_Completos []){
    uint8_t i;
    printf("***********VECINOS***********\n");
    imprimir_heap(Vecinos);
    printf("------------------------------\n");
    printf("Vecinos: [ ");
    for(i = 0; i<MAX_ELEMENT; i++){
        printf("%d  ",Vecinos_Completos[i]);
    }
    printf("]\n");
    printf("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.\n");
}

void Imprimir_Propuestas(Heap Propuestas, uint16_t Propuestas_Completas[][5]){
    uint8_t i, j;
    printf("***********PROPUESTAS***********\n");
    imprimir_heap(Propuestas);
    printf("------------------------------\n");
    printf("Propuestas: [");
    for(i = 0; i< 10; i++){
        for(j = 0; j<5; j++){
            printf("%d  ", Propuestas_Completas[i][j]);
        }   
    printf("],[");
    }
    printf("]\n");
    printf("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.\n");
}

void reset_vector(int Vector[]){
    int i;
    for(i = 0; i< MAX_ELEMENT; i++){
        Vector[i] = -1;
    }
}

void Inicializar_Vector_Vecinos(Elemento vector[], uint8_t tam){
    int i;
    for(i = 0; i<tam; i++){
        Elemento e = {-1,-1};
        vector[i] = e;
    }
}

int max(int num1, int num2){
    if(num1>= num2){
        return num1;
    }
    else{
        return num2;
    }
}

uint8_t Comparar_Formacion(uint8_t Vector1[], uint16_t Vector2[]){
    /*printf("Vector1: %d %d Vector 2: %d %d\n",Vector1[0], Vector1[1], Vector2[0], Vector2[1]);*/
    return((Vector1[0] == Vector2[0] && Vector1[1] == Vector2[1])|| (Vector1[0] == Vector2[1] && Vector1[1] == Vector2[0]));
}

uint8_t Propuesta(uint8_t Propuesta[3], uint8_t Vector[2]){
    Vector[0] = Propuesta[0];
    if(Propuesta[1] == kilo_uid){
        Vector[1] = Propuesta[2];
        return 1;
    }
    else if(Propuesta[2] == kilo_uid){
        Vector[1] = Propuesta[1];
        return 1;
    }
    else{
        return 0;
    }
}


void setup() {
    rand_seed(rand_hard()); 
    aleatorio = rand_soft();
    aleatorio_rediscover = rand_soft();
    last_update = kilo_ticks;
    id = kilo_uid;

    Vecinos = create_Heap(0);
    Propuestas = create_Heap(1);
    Inicializar_Vecinos(Lista_Vecinos);
    Inicializar_Propuestas(Lista_Propuestas);



    msg.type = NORMAL;
    msg.data[0] = id;
    msg.crc = message_crc(&msg);
}

void Discover(){
    uint8_t var_aleatoria, num_iteraciones;
    num_iteraciones = 0;
    while(num_iteraciones <10){
        /*printf(".-.-.-.-.-.-.-.-.\n");
        printf("Iteracion numero %d \n", num_iteraciones);*/
    if (state == 1){
        var_aleatoria = aleatorio;
        /*printf("He elegido el aleatorio\n");*/
    }
    else if(state == 2){
        var_aleatoria = aleatorio_rediscover;
        /*printf("He elegido el aleatorio rediscover\n");*/
    }
    /*printf("Aleatorio vale %d\n",var_aleatoria);*/
    aleatorio_alternancia = rand_soft();
    /*printf("Aleatorio de alternancia vale %d\n", aleatorio_alternancia);*/
    if(var_aleatoria > 128  && aleatorio_alternancia > 128){
        delay(rand_soft()); 
        if (message_sent) {
            message_sent = 0;
            /*printf("Mensaje enviado con exito\n");*/
            set_color(RGB(1,0,1));
        }
        var_aleatoria = 0;   
    }
    else{
        set_color(RGB(0,0,0));
        /*printf("Esperando a recibir un mensaje.....\n");*/
        if (new_message) {
            dist = estimate_distance(&dist_measure);
           /* printf("La distancia es: %d\n", dist);
            printf("Mensaje recibido con exito\n");*/
            Elemento e = {data[0],dist};
            push(&Vecinos,e);
            Lista_Vecinos[Vecinos.num_elem] = data[0];
            printf("El numero de elementos de Vecinos es %d \n",Vecinos.num_elem);
            set_color(RGB(1,1,0));
            delay(100);
            set_color(RGB(0,0,0));
            new_message = 0;
        }
        if(var_aleatoria > 128){
            var_aleatoria = 0;
        }
        else{
            var_aleatoria = (uint8_t) 130;
        }
    }
    /*printf("Aleatorio vale %d\n",var_aleatoria);*/
    if (state == 1){
        aleatorio = var_aleatoria;
        /*printf("He modificado el aleatorio\n");*/
    }
    else if(state == 2){
        aleatorio_rediscover = var_aleatoria;
        /*printf("He modificado el aletorio rediscover\n");*/
    }
    num_iteraciones = num_iteraciones +1;
    /*printf(".-.-.-.-.-.-.--.-.\n");*/
}
}

uint8_t Seleccion_Configuracion(){
    int aleatorio_movimiento, i, j, k, num_iter, tam, rellenar_en, contador_propuestas, resultado;
    num_iter = 0;
    resultado = 0;
while(num_iter < 10 && kilo_ticks<last_update+300){
    printf("-.-.-.-.-.-.-.\n");
    printf("Iteracion numero %d\n", num_iter);
    printf("El numero de vecinos es %d \n",Vecinos.num_elem);
    if(Vecinos.num_elem <1 ){
        aleatorio_movimiento = rand_soft();
        printf("El aleatorio de movimiento es %d\n", aleatorio_movimiento);
        if(aleatorio_movimiento < 85){
            spinup_motors();
            set_motors(kilo_turn_left, 0);
            delay(3000);
            set_motors(0,0);
            printf("Me muevo hacia la izquierda\n");
            reset_heap(&Vecinos);
            reset_vector(Lista_Vecinos);
            Discover();
            printf("He vuelto de Discover\n");
            
        }
        else if(aleatorio_movimiento < 170){
            spinup_motors();
            set_motors(0, kilo_turn_right);
            delay(3000);
            set_motors(0,0);
            printf("Me muevo hacia la derecha\n");
            reset_heap(&Vecinos);
            reset_vector(Lista_Vecinos);
            Discover();
            printf("He vuelto de Discover\n");
            
        }
        else{
            spinup_motors();
            set_motors(kilo_turn_left, kilo_turn_right);
            delay(3000);
            set_motors(0,0);
            printf("Me muevo hacia delante\n");
            reset_heap(&Vecinos);
            reset_vector(Lista_Vecinos);
            Discover();
            printf("He vuelto de Discover\n");            

        }

    }
    else{
        /*printf("Tengo mas de dos vecinos\n");*/
        set_motors(0,0);
        set_color(RGB(0,0,0));
        delay(500);
        set_color(RGB(1,1,1));
        resultado = 1;

    }
    num_iter = num_iter +1;
    printf(".-.-.-.-.-\n");
 }
    if(num_iter == 10){
        /*printf("Ya llevo diez\n");*/
        tam = Vecinos.num_elem +1;
        printf("Hay %d vecinos \n", tam);
        /*Elemento Vecinos_Robot [tam];
        for(i = 0; i< tam; i++){
            Elemento e = {-1,-1};
            Vecinos_Robot[i] = e;
        }
        for(i = 0; i< tam; i++){
            imprimir_elemento(Vecinos_Robot[i]);
            printf("\n");
        }
        rellenar_en = 0;
        while(Vecinos.num_elem != -1){
            Elemento temporal = pop(&Vecinos);
            printf("El temporal es \n");
            imprimir_elemento(temporal);
            printf("\n");
            Vecinos_Robot[rellenar_en] = temporal;
            rellenar_en = rellenar_en +1;
        }
        for(i = 0; i< tam; i++){
            imprimir_elemento(Vecinos_Robot[i]);
            printf("\n");
        }  */
        contador_propuestas = 0;
        for(i=0;i<tam-1;i++){
            for(j=i+1;j<tam;j++){
                /*printf("i y j valen %d y %d \n",i,j );
                printf("Los valores a introducir son %d y %d\n", get_key(Vecinos.datos[i]), get_key(Vecinos.datos[j]));
                printf("Los valores a introducir son %d y %d\n", max(get_key(Vecinos.datos[i]), get_key(Vecinos.datos[j])), (10-max(get_key(Vecinos.datos[i]), get_key(Vecinos.datos[j]))*1));*/
                Lista_Propuestas[contador_propuestas][0] = get_ref(Vecinos.datos[i]);
                Lista_Propuestas[contador_propuestas][1] = get_ref(Vecinos.datos[j]);
                Lista_Propuestas[contador_propuestas][2] = 1;
                Lista_Propuestas[contador_propuestas][3] = max(get_key(Vecinos.datos[i]), get_key(Vecinos.datos[j]));
                Lista_Propuestas[contador_propuestas][4] = (K-max(get_key(Vecinos.datos[i]), get_key(Vecinos.datos[j]))*1);
                Elemento Propuesta = {contador_propuestas, Lista_Propuestas[contador_propuestas][4]};
                push(&Propuestas,Propuesta);
                contador_propuestas = contador_propuestas + 1;
            }
        }


        /*for(i = 0; i<tam; i++){
            for(j = 0; j< tam; j++){
                if(j>=i && i!=j){
                    printf("i y j valen %d y %d \n",i,j );
                    printf("Los valores a introducir son %d y %d\n", get_key(Vecinos_Robot[i]), get_key(Vecinos_Robot[j]));
                    printf("Los valores a introducir son %d y %d\n", max(get_key(Vecinos_Robot[i]), get_key(Vecinos_Robot[j])), (10-max(get_key(Vecinos_Robot[i]), get_key(Vecinos_Robot[j]))*1));
                    Lista_Propuestas[contador_propuestas][0] = get_ref(Vecinos_Robot[i]);
                    Lista_Propuestas[contador_propuestas][1] = get_ref(Vecinos_Robot[j]);
                    Lista_Propuestas[contador_propuestas][2] = 1;
                    Lista_Propuestas[contador_propuestas][3] = max(get_key(Vecinos_Robot[i]), get_key(Vecinos_Robot[j]));
                    Lista_Propuestas[contador_propuestas][4] = (100-max(get_key(Vecinos_Robot[i]), get_key(Vecinos_Robot[j]))*1);
                    Elemento Propuesta = {contador_propuestas, Lista_Propuestas[contador_propuestas][4]};
                    push(&Propuestas,Propuesta);
                    contador_propuestas = contador_propuestas + 1;
                }
            }
        }*/
    }
    return resultado;
}

void Consenso(){
    uint16_t iteraciones_consenso = 0;
    uint8_t i;
    uint16_t marcador_votos = 0;
    uint8_t Propuesta_Obtenida[2];
    uint16_t new_fitness;
    Propuesta_Elegida = 0;
    rand_seed(rand_hard());
    aleatorio = rand_soft();
    while(iteraciones_consenso < 10){
        aleatorio_alternancia = rand_soft();
        printf("Iteracion numero: %d \n", iteraciones_consenso);
        msg.data[0] = kilo_uid;
        msg.data[1] = Lista_Propuestas[Propuesta_Elegida][0];
        msg.data[2] = Lista_Propuestas[Propuesta_Elegida][1];
        msg.crc = message_crc(&msg);
        printf("Aleatorio y aleatorio de alternancia valen %d %d \n", aleatorio, aleatorio_alternancia);
        if(aleatorio >= 128 && aleatorio_alternancia >= 128){
            delay(rand_soft()); 
            if(message_sent){
                set_color(RGB(1,0,1));
                printf("Propuesta enviada %d %d %d \n",msg.data[0], msg.data[1], msg.data[2]);
            }
            aleatorio = 0;
        }
        else{
            set_color(RGB(1,0,1));
            printf("Esperando a recibir mensaje\n");
            if (new_message) {
                uint8_t r1, r2, r3, j, comp;    
                comp = 0;                   
                r1 = data[0];
                r2 = data[1];
                r3 = data[2];
                uint8_t Propuesta_Recibida [3] = {r1, r2, r3};
                uint8_t MiPropuesta [3] = {kilo_uid,0,0};
                set_color(RGB(1,1,0));
               /* printf("Mensaje recibido %d %d %d \n",data[0], data[1], data[2]);
                printf("%d %d %d \n",Propuesta_Recibida[0], Propuesta_Recibida[1],Propuesta_Recibida[2]);*/
                if(Propuesta(Propuesta_Recibida, Propuesta_Obtenida)){
                    marcador_votos = marcador_votos + 1;
                    printf("Marcador votos: %d Propuesta recibida: %d %d  %d \n Obtenida: %d %d ", marcador_votos, Propuesta_Recibida[0],Propuesta_Recibida[1],Propuesta_Recibida[2], Propuesta_Obtenida[0],Propuesta_Obtenida[1]);
                    for(i=0;i<Propuestas.num_elem + 1;i++){
                        /*printf("Propuesta obtenida: %d %d \n Propuesta mia: %d %d ",Propuesta_Obtenida[0],Propuesta_Obtenida[1], Lista_Propuestas[i][0], Lista_Propuestas[i][1]);*/
                        Comparar_Formacion(Propuesta_Obtenida,Lista_Propuestas[i]);
                        if(Comparar_Formacion(Propuesta_Obtenida,Lista_Propuestas[i])){
                            Lista_Propuestas[i][2] = Lista_Propuestas[i][2] + 1;
                            printf("Sumo\n");
                        }
                        else{
                            if(Lista_Propuestas[i][2] > 0){
                                Lista_Propuestas[i][2] = Lista_Propuestas[i][2] - 1;
                                printf("Resto y me quedo con %d votos\n", Lista_Propuestas[i][2]);
                            }
                        }
                        Lista_Propuestas[i][4] = (K - Lista_Propuestas[i][3])*Lista_Propuestas[i][2];
                        new_fitness = (K - Lista_Propuestas[i][3])*Lista_Propuestas[i][2];
                        printf("Calculo nuevo fitnes igual a %d \n", (K - Lista_Propuestas[i][3])*Lista_Propuestas[i][2]);
                    }
                }
                /*for(i = 0; i<10; i++){
                    MiPropuesta[1] = Lista_Propuestas[i][0];
                    MiPropuesta[2] = Lista_Propuestas[i][1];
                    printf("%d %d \n",MiPropuesta[1], MiPropuesta[2]);
                    comp = Comparar(MiPropuesta,Propuesta_Recibida);
                    if(comp == 1){
                        Lista_Propuestas[i][2] = Lista_Propuestas[i][2] + 1;
                        Lista_Propuestas[i][4] = (100 - Lista_Propuestas[i][3])*Lista_Propuestas[i][2];
                    }
                    else{
                        if(Lista_Propuestas[i][2] > 0){
                            Lista_Propuestas[i][2] = Lista_Propuestas[i][2] - 1;
                            Lista_Propuestas[i][4] = (100 - Lista_Propuestas[i][3])*Lista_Propuestas[i][2];
                        }
                    }
                }*/
           }

            if(aleatorio > 128){
                aleatorio = 0;
            }
            else{
                aleatorio = 130;
            }
        }
        uint8_t numero_propuestas = Propuestas.num_elem + 1;
        /*printf("Numero de propuestas que hay que meter: %d \n",numero_propuestas);*/
        reset_heap(&Propuestas);
        /*Propuestas.num_elem = -1;*/
        for(i = 0; i<numero_propuestas; i++){
            Elemento poner = {i, Lista_Propuestas[i][4]};
            printf("Voy a poner el elemento:");
            imprimir_elemento(poner);
            printf("\n");
            push(&Propuestas, poner);
        }
        Imprimir_Propuestas(Propuestas, *Lista_Propuestas);
        Elemento Primera_Propuesta = peak(&Propuestas);
        Propuesta_Elegida = get_ref(Primera_Propuesta);
        imprimir_elemento(Primera_Propuesta);
        iteraciones_consenso = iteraciones_consenso + 1;
        delay(2000);
    }
}
        

void Lucernario(){
   uint8_t p,i, luces, vecino_1, vecino_2, iteraciones_formacion, recibidos, ack1, ack2;
   ack1 = 0;
   ack2 = 0;
    recibidos = 0;
    uint8_t Propuesta_Obtenida[2];
    iteraciones_formacion = 0;
    p = Propuesta_Elegida;
    vecino_1 = Lista_Propuestas[p][0];
    vecino_2 = Lista_Propuestas[p][1];
            msg.type = NORMAL;
            msg.data[0] = kilo_uid;
            msg.data[1] = Lista_Propuestas[p][0];
            msg.data[2] = Lista_Propuestas[p][1];
            msg.crc = message_crc(&msg);
            uint8_t Formacion[2] = {-1,-1};
            aleatorio = rand_soft();
           while(iteraciones_formacion <100){
            /*printf("Iteracion %d\n",iteraciones_formacion);*/
            aleatorio_alternancia = rand_soft();
            /*printf("Aleatorio y alternancia valen %d %d\n",aleatorio, aleatorio_alternancia );*/
            if(aleatorio >= 128 && aleatorio_alternancia >= 128){
                delay(rand_soft()); 
                if(message_sent){
                    set_color(RGB(1,0,1));
                }
                aleatorio = 0;
            }
            else{
                set_color(RGB(0,0,0));
                /*printf("Esperando a recibir mensaje\n");*/
                if(new_message){
                    set_color(RGB(1,1,0));
                    uint8_t contenido[3] = {data[0],data[1],data[2]};
                    delay(100);
                    set_color(RGB(1,1,0));
                    uint8_t mi_propuesta[3] = {kilo_uid,Lista_Propuestas[p][0],Lista_Propuestas[p][1]};
                    if(Propuesta(contenido, Propuesta_Obtenida)){
                        if(Comparar_Formacion(Propuesta_Obtenida,Lista_Propuestas[get_ref(peak(&Propuestas))])){
                            /*printf("Propuesta Obtenida: %d %d y Propuesta de la lista %d %d\n",Propuesta_Obtenida[0], Propuesta_Obtenida[1], Lista_Propuestas[get_ref(peak(&Propuestas))][0], Lista_Propuestas[get_ref(peak(&Propuestas))][1] );*/
                            if(contenido[0] == vecino_1){
                                ack1 = 1;
                            }
                            else if(contenido[0] == vecino_2){
                                ack2 = 1;
                            }
                        }
                    }
                aleatorio = 130;
            }
        }
        iteraciones_formacion = iteraciones_formacion +1;
    }
            if(ack1 == 1 && ack2 == 1){
                set_color(RGB(0,1,1));
                delay(3000);
            }
            else{
                while(1){
                    set_color(RGB(0,0,0));
                    set_motors(0,0);
                    printf("Mi propuesta no ha sido consensuada\n");
                }
            }

}


void Seleccion_Lider(){
    uint8_t Control_Voto [2] = {0,0};
    uint8_t mask = 00000001;
    uint8_t voto;
    uint8_t votomio = 0;
    uint8_t iteraciones = 0;
    uint8_t p, i, max;
    uint8_t num_votos = 0;
    uint8_t Votos [3] = {0,0,0};
    p = Propuesta_Elegida;
    aleatorio = rand_soft();
    aleatorio = aleatorio && mask;
    if(aleatorio == 0){
        voto = Lista_Propuestas[p][0];
    }
    else{
        voto = Lista_Propuestas[p][1];
    }
    msg.type = NORMAL;
    msg.data[0] = kilo_uid;
    msg.data[1] = voto;
    msg.crc = message_crc(&msg);
    aleatorio = rand_soft();
    while(iteraciones < 20){
        printf("Iteracion numero %d\n",iteraciones);
        aleatorio_alternancia = rand_soft();
        if(aleatorio >= 128 && aleatorio_alternancia >= 128){
            if(message_sent){
                set_color(RGB(1,0,1));
                delay(100);
                printf("Voto a %d\n",voto );
                if(votomio == 0){
                    if(voto == Lista_Propuestas[p][0]){
                        Votos[1] = Votos[1] + 1;
                    }
                    else if(voto == Lista_Propuestas[p][1]){
                        Votos[2] = Votos[2] + 1;
                    }
                    votomio = 1;
                }
            }
            aleatorio = 0;
        }
        else{
            if(new_message){
                set_color(RGB(1,1,0));
                delay(100);
                set_color(RGB(0,0,0));
                uint8_t Voto_Valido = 0;
                if(data[0] == Lista_Propuestas[p][0] && !Control_Voto[0]){
                    Control_Voto[0] = 1;
                    Voto_Valido = 1;
                }
                else if(data[0] == Lista_Propuestas[p][1] && !Control_Voto[1]){
                    Control_Voto[1] = 1;
                    Voto_Valido = 1;
                }
                else{
                    Voto_Valido = 0;
                    printf("Voto no valido de %d\n", data[0]);
                }
                if(Voto_Valido){
                    if(data[1] == kilo_uid){
                        Votos[0] = Votos[0] + 1;
                        printf("Voto para mi de %d \n",data[0]);
                    }
                    else if(data[1] == Lista_Propuestas[p][0]){
                        Votos[1] = Votos[1] + 1;
                        printf("Voto al primero para %d de %d \n",Lista_Propuestas[p][0], data[0]);
                    }
                    else if(data[1] == Lista_Propuestas[p][1]){
                        Votos[2] = Votos[2] + 1;
                        printf("Voto al segundo para %d de %d \n",Lista_Propuestas[p][1], data[0]);
                    }
                }
            }
            aleatorio = 130;
        }
        iteraciones = iteraciones +1;
    }
    max = Votos[0];
    uint8_t lider = kilo_uid;
    for(i = 0; i<3;i++){
        if(Votos[i]>max){
            max = Votos[i];
            if(i == 0){
                lider = kilo_uid;
            }
            else{
                lider = Lista_Propuestas[p][i-1];
            }
        }
    }
    while(1){
        set_color(lider);
        delay(500);
        set_color(RGB(0,0,0));
        delay(500);
        printf("Votos: %d %d %d %d %d %d \n El lider es %d \n", kilo_uid, Votos[0], Lista_Propuestas[p][0], Votos[1], Lista_Propuestas[p][1], Votos[2], lider);
    }
}

void loop() {
    act_state = change_state(kilo_ticks);
    delay(3000);
    if(act_state){
        switch(state){
            case INIT_AMARILLO:
            set_color(RGB(1,1,0));
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            /*printf("INCIALIZADO\n");*/
            /*Imprimir_Vecinos(Vecinos, Lista_Vecinos);
            Imprimir_Propuestas(Propuestas, *Lista_Propuestas);*/
            /*delay(10000);*/
            /*printf("INCIALIZADO\n");*/
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            break;

            case DISCOVER_ROJO:
            while(kilo_ticks < last_update + 320){
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            printf("INICIO DESCUBRIMIENTO\n");
            set_color(RGB(1,0,0));
            Discover();
            Imprimir_Vecinos(Vecinos, Lista_Vecinos);
            Imprimir_Propuestas(Propuestas, *Lista_Propuestas);
            printf("DESCUBRIMIENTO FINALIZADO\n");
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            /*delay(5000);*/
            }
            set_color(RGB(0,0,0));
            last_update = kilo_ticks;
            break;

            case SELECT_CONFIGURATION_BLANCO:
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            printf("COMIENZA LA SELECCION DE LA CONFIGURACION\n");
            uint8_t resultado = 0;
            resultado = Seleccion_Configuracion(&Vecinos);
            while(kilo_ticks < last_update + 320){
            set_color(RGB(1,1,1));
            if(resultado == 1){
                Imprimir_Vecinos(Vecinos, Lista_Vecinos);
                Imprimir_Propuestas(Propuestas, *Lista_Propuestas);
                /*printf("CONFIGURACION SELECCIONADA\n");
                printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            }
            else{
                resultado = Seleccion_Configuracion(&Vecinos);
            }
            if(Vecinos.num_elem < 1){
                while(1){
                    set_motors(0,0);
                    set_color(RGB(0,0,0));
                }
            }
            }
            last_update = kilo_ticks;
            break;

            case CONSENSO_AZUL:
            set_motors(0,0);
            set_color(RGB(0,0,1));
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            printf("CONSENSO\n");
            Consenso();
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            /*Imprimir_Vecinos(Vecinos, Lista_Vecinos);*/
            printf("CONSENSO TERMINADO\n");
            /*while(1){
                Imprimir_Propuestas(Propuestas, *Lista_Propuestas);
                delay(1000);
            }*/
            last_update = kilo_ticks;
            break;

            case LUCERNARIO_VERDE:
           /* set_color(RGB(0,1,0));*/
           /* while(kilo_ticks < last_update + 400){*/
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            printf("LUCERNARIO\n");
            Lucernario();
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            /*}*/
            last_update = kilo_ticks;
            break;

            case LIDER:
            /*while(kilo_ticks < last_update + 300){*/
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
            printf("Lider\n");
            Seleccion_Lider();
            /*printf("¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨'¨\n");*/
           /* }*/
            last_update = kilo_ticks;
            break;


        }

        act_state = 0;
    }
}

int main() {
    // initialize hardware
    kilo_init();
    // register your program
    debug_init();

    change_state(kilo_ticks);
    Imprimir_Vecinos(Vecinos, *Lista_Vecinos);
    Imprimir_Propuestas(Propuestas, *Lista_Propuestas);
    kilo_message_rx = message_rx;
    kilo_message_tx = message_tx;
    kilo_message_tx_success = message_tx_success;
    kilo_start(setup, loop);


    return 0;
}