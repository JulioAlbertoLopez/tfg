import matplotlib.pyplot as plt 
import numpy as np
import math
import random
import networkx as nx
from collections import deque

class Robot:
	identificador = 0
	posicion_x = 0
	posicion_y = 0
	Vecinos = {}
	Buzon = deque([])
	Propuesta = []
	Tabla_Propuesta = []
	Rol = 0
	
	def __init__(self, identificador, posicion_x, posicion_y, Vecinos, Buzon, Propuesta, Tabla_Propuesta):
		self.identificador = identificador
		self.posicion_x = posicion_x
		self.posicion_y = posicion_y
		self.Vecinos = Vecinos
		self.Buzon = Buzon
		self.Propuesta = Propuesta	
		self.Tabla_Propuesta = Tabla_Propuesta
	
	def __move__(self):
		self.posicion_x = int(random.uniform(-1,1)*3)
		self.posicion_y = int(random.uniform(-1,1)*3)
	
	def __str__(self):
		print "Robot: "+str(self.identificador)+" --> Posicion_EjeX: (" +str(self.posicion_x)+ ") Posicion_EjeY: (" +str(self.posicion_y)+ ") --> " +str(self.identificador)+":("+str(self.posicion_x)+","+str(self.posicion_y)+")"
		print "Vecinos:"
		for (clave,valor) in self.Vecinos.items():
			print str(clave)+' '+str(valor)+' - \n'
		print "Mensajes:"
		for i in range(len(self.Buzon)):
			print self.Buzon[i]
		print "Propuesta:"
		for i in range(len(self.Propuesta)):
			print self.Propuesta[i]
		print "Tabla de Propuestas:"
		for i in range(len(self.Tabla_Propuesta)):
			print self.Tabla_Propuesta[i]
		return ""
		


class Mensaje:
	id_msg = 0
	origen = 0
	destino = 0
	contenido = []
	
	def __init__(self, origen, destino, contenido):
		self.id_msg = random.randrange(100)
		self.origen = origen
		self.destino = destino
		self.contenido = contenido
	
	def send_message(self, Robot_Origen, Robot_Destino, contenido_msg):
		self.origen = Robot_Origen.identificador
		self.destino = Robot_Destino.identificador
		self.contenido = contenido_msg
		Robot_Destino.Buzon.append(self)
		
	def receive_message(self, Robot):
		message = Robot.Buzon.pop()
		return message
		
	def __str__(self):
		print "Mensaje: " +str(self.id_msg)+" Origen: "+str(self.origen)+ " Destino: "+str(self.destino)
		print "Contenido."
		for i in range (len(self.contenido)):
			print self.contenido[i]
		return ""


def sin_repetir(x):
	for i in range(len(x)):
		for j in range((len(x))):
			if (i!=j):
				if(x[i] == x[j]):
					aux = np.random.randint(0,20)
					if(aux not in x):
						x[i] = aux 
					else:
						while(aux in x):
							aux = np.random.randint(0,20)
						x[i] = aux
	return x	
	
def Descubrimiento(ListaRobots):
	Vecinos = {}
	for i in range(len(ListaRobots)):
		Distancias_robot = {}
		for j in range(len(ListaRobots)):
			if(i!=j):
				Distancia_individual = {}
				if(abs(ListaRobots[i].posicion_x- ListaRobots[j].posicion_x)<10 and abs(ListaRobots[i].posicion_y - ListaRobots[j].posicion_y)<10 ):
					Distancia_individual["manhattan"] = abs(ListaRobots[i].posicion_x- ListaRobots[j].posicion_x)
					+ abs(ListaRobots[i].posicion_y - ListaRobots[j].posicion_y)
					Distancia_individual["euclidea"] = math.sqrt(((ListaRobots[i].posicion_x - ListaRobots[j].posicion_x)**2)+ abs((ListaRobots[i].posicion_y - ListaRobots[j].posicion_y)**2))
					Distancias_robot[j] = Distancia_individual
					G.add_edge(i,j,weight = Distancia_individual["euclidea"])
					msg_contenido = []
					msg_contenido.append(Distancia_individual["manhattan"])
					msg_contenido.append(Distancia_individual["euclidea"])
					msg = Mensaje(ListaRobots[i].identificador,ListaRobots[j].identificador,msg_contenido)
					msg.send_message(ListaRobots[i],ListaRobots[j],msg_contenido)
				

		Vecinos[i] = Distancias_robot
		ListaRobots[i].Vecinos = Vecinos[i]

def Plot_Kilobots(vector_x, vector_y):
	plt.figure('Kilobots-Posicion') 
	plt.scatter(x,y)
	plt.show()

def Plot_Grafo(Grafo):
	plt.figure('Grafo Kilobots')
	nx.draw_networkx_labels(G,pos = nx.spring_layout(G),font_size=20,font_family='sans-serif')
	nx.draw(G)
	plt.show()

def Configuracion(ListaRobots):
	for i in range(len(ListaRobots)):
		Mensajes = ListaRobots[i].Buzon
		Vecinos = ListaRobots[i].Vecinos
		Lista_Vecinos = Vecinos.keys()
		if(len(Lista_Vecinos)<2):
			ListaRobots[i].__move__()
			"Me muevo con el robot",ListaRobots[i].identificador
			Descubrimiento(ListaRobots)
			Configuracion(ListaRobots)
		Fitness = {}
		Propuesta = []
		T_Propuestas = []
		print "Robot: ",i
		while(Mensajes):
			msg = Mensaje(0,0,[])
			msg = msg.receive_message(ListaRobots[i])
			cont = msg.contenido
			#msg = Mensajes.pop()
			#cont = msg.contenido
			Fitness [msg.origen] = (cont[0]+cont[1])/2
		ListaRobots[i].Tabla_Propuesta = T_Propuestas
		ListaRobots[i].Propuesta = Propuesta
		for clave, valor in Fitness.items():
			print(clave, '-->', valor)
		for i in range(len(Lista_Vecinos)):
			for j in range(len(Lista_Vecinos)):
				if i != j and j>i:
					T_Propuestas.append([Lista_Vecinos[i], Lista_Vecinos[j],0, Fitness[Lista_Vecinos[i]]+Fitness[Lista_Vecinos[j]],[msg.destino,Lista_Vecinos[i],Lista_Vecinos[j]]])
		T_Propuestas.sort(key=lambda x:x[3])  
		Propuesta.append(T_Propuestas[0][4])
		print "Su propuesta es:"
		for k in range(len(Propuesta)):
			print Propuesta[k]

def Asignar_Roles(Lista):
	for i in range(len(Lista)):
		rol = random.randint(0,1)
		Lista[i].Rol = rol
		print "El rol del robot" ,i, " es: ", Lista[i].Rol 


def Enviar_Propuesta(Robot):
	contenido = []
	Tabla = Robot.Tabla_Propuesta
	contenido.append(Tabla[0][4])
	msg = Mensaje(Robot.identificador,contenido[0][1],contenido)
	msg2 = Mensaje(Robot.identificador,contenido[0][2],contenido)
	Lista_Mensajes.append(msg)
	Lista_Mensajes.append(msg2)
	

def Comparar(lista1, lista2):
	"""for i in range(len(lista1)):
		print lista1[i]
	for i in range(len(lista2)):
		print lista2[i]"""
	igual = 1
	for i in range(len(lista1[0])):
		if(lista1[0][i] == lista2[0][0] or lista1[0][i] == lista2[0][1] or lista1[0][i] == lista2[0][2]):
			#print lista1[0][i], lista2[0][0], lista2[0][1], lista2[0][2]  
			i+=1
		else:
			igual = 0
	return igual
			
	
def Leer_Propuesta(Robot):
	for i in range(len(Lista_Mensajes)-1,-1,-1):
		if(Lista_Mensajes[i].destino == Robot.identificador):
			#print Lista_Mensajes[i].destino, Robot.identificador
			mensaje = Lista_Mensajes[i]
			del Lista_Mensajes[i]
			contenido_mensaje = mensaje.contenido
			#for k in range(len(contenido_mensaje)):
				#print contenido_mensaje[k]
			for j in range(len(Robot.Tabla_Propuesta)):
				prop = []
				prop.append(Robot.Tabla_Propuesta[j][4])
				igual = Comparar(contenido_mensaje, prop)
				#print igual
				if(igual == 1):
					Robot.Tabla_Propuesta[j][2]+=1
	Robot.Tabla_Propuesta.sort(key=lambda x:x[2], reverse = True) 	

def swap_roles(ListaR):
	for i in range(len(ListaR)):
		if(ListaR[i].Rol == 1):
			ListaR[i].Rol = 0
		else:
			ListaR[i].Rol = 1
		print "El rol del robot",i ,"es", ListaR[i].Rol
			

def Consenso(ListaRobots, Lista_Mensajes):
	Asignar_Roles(ListaRobots)
	for i in range(25):
		for i in range(len(ListaRobots)):
			if(ListaRobots[i].Rol == 1):
				Enviar_Propuesta(ListaRobots[i])
			
		for i in range(len(Lista_Mensajes)):
			print Lista_Mensajes[i]

		for i in range(len(ListaRobots)):
			if(ListaRobots[i].Rol == 0):
				Leer_Propuesta(ListaRobots[i])	
	
		swap_roles(ListaRobots)	






G = nx.Graph()
ListaRobots = []
Lista_Mensajes = []

vector = np.random.randint(0,20,(10,1))
x = sin_repetir(vector) 
y = np.random.randint(0,20,(10,1))

"""******************* INICIALIZACION KILOBOTS *******************"""
print "******************* INICIALIZACION KILOBOTS *******************"
for i in range (10):
	Vec = {}
	Buzon_msg = deque([])
	Propuesta_Robot = []
	Tabla_Propuesta = []
	R = Robot(i,x[i],y[i],Vec,Buzon_msg, Propuesta_Robot, Tabla_Propuesta)
	ListaRobots.append(R)
	G.add_node(i)
	print R

Descubrimiento(ListaRobots)

for i in range(len(ListaRobots)):
	print ListaRobots[i]

print "Nodos: ", G.number_of_nodes(), G.nodes()
print "Enlaces: ", G.number_of_edges(),G.edges()

Plot_Kilobots(x,y)
Plot_Grafo(G)

"""-.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.-"""
"""**************************************************FASE 2. CONSENSO Y CONFIGURACION DE EQUIPOS****************************************"""
"""-.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.--.-.-.-.-.-.-.-.-.-"""		
Configuracion(ListaRobots)

for i in range (len(ListaRobots)):
	print ListaRobots[i]

Consenso(ListaRobots, Lista_Mensajes)
	
for i in range(len(ListaRobots)):
	print ListaRobots[i]
