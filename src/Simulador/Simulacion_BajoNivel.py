import matplotlib.pyplot as plt 
import numpy as np
import math
import random
import networkx as nx
import time
from collections import deque

class Heap:
	
	def __init__(self):
		self.Heap = []

	def __init__(self, Lista):
		self.Heap = Lista
		heapsort(Lista)
	
	def __push__(self, elemento):
		self.Heap.append(elemento)
		heapsort(self.Heap)
	
	def __pop__(self):
		elemento = self.Heap[len(self.Heap)-1]
		del self.Heap[len(self.Heap)-1]
		heapsort(self.Heap)
		return elemento
		
	def __getitem__(self,i):
		elemento = self.Heap[i]
		return elemento
	
	def __head__(self):
		elemento = self.Heap[0]
		return elemento
	
	def __len__(self):
		return len(self.Heap)
	
	def __str__(self):
		for i in range(len(self.Heap)):
			print self.Heap[i]
		return ""


def heapsort( Lista ):
	length = len( Lista ) - 1
	padre = length / 2
	for i in range ( padre, -1, -1 ):
		heap( Lista, i, length )
	for i in range ( length, 0, -1 ):
		if Lista[0].key > Lista[i].key:
			swap( Lista, 0, i )
			heap( Lista, 0, i - 1 )
				
def heap( Lista, primero, ultimo ):
	hijo_derecho = 2 * primero + 1
	while(hijo_derecho <= ultimo):
		if ( hijo_derecho < ultimo ) and ( Lista[hijo_derecho].key < Lista[hijo_derecho + 1].key ):
			hijo_derecho += 1
		if Lista[hijo_derecho].key > Lista[primero].key:
			swap( Lista, hijo_derecho, primero )
			primero = hijo_derecho;
			hijo_derecho = 2 * primero + 1
		else:
			return 
			
def swap( Lista, i, j ):
	aux = Lista[i]
	Lista[i] = Lista[j]
	Lista[j] = aux



class Robot:
	identificador = 0
	posicion_x = 0
	posicion_y = 0
	Vecinos = Heap([])
	Buzon = deque([])
	Propuesta_Robot = []
	Propuestas = Heap([])
	Rol = 0
	Sleep = 0
	
	def __init__(self, identificador, posicion_x, posicion_y):
		self.identificador = identificador
		self.posicion_x = posicion_x
		self.posicion_y = posicion_y
	
	def __move__(self):
		self.posicion_x = int(random.uniform(-1,1)*3)
		self.posicion_y = int(random.uniform(-1,1)*3)
		
	def __sleep__(self, valor):
		self.sleep = valor
	
	def __str__(self):
		print "Robot: "+str(self.identificador)+" --> Posicion_EjeX: (" +str(self.posicion_x)+ ") Posicion_EjeY: (" +str(self.posicion_y)+ ") --> " +str(self.identificador)+":("+str(self.posicion_x)+","+str(self.posicion_y)+")"
		print "Vecinos:"
		self.Vecinos.__str__()
		print "Mensajes:"
		for i in range(len(self.Buzon)):
			print self.Buzon[i]
		print "Propuesta:"
		print self.Propuesta_Robot
		print "Tabla de Propuestas:"
		for i in range(len(self.Propuestas)):
			print self.Propuestas[i],T_Propuestas[self.identificador][self.Propuestas[i].ref]
		return ""

class Mensaje:
	id_msg = 0
	origen = 0
	destino = 0
	contenido = []
	
	def __init__(self, origen, destino, contenido):
		self.id_msg = random.randrange(100)
		self.origen = origen
		self.destino = destino
		self.contenido = contenido
	
	def send_message(self, Robot_Origen, Robot_Destino, contenido_msg):
		self.origen = Robot_Origen.identificador
		self.destino = Robot_Destino.identificador
		self.contenido = contenido_msg
		Robot_Destino.Buzon.append(self)
		
	def receive_message(self, Robot):
		message = Robot.Buzon.pop()
		return message
		
	def __str__(self):
		print "Mensaje: " +str(self.id_msg)+" Origen: "+str(self.origen)+ " Destino: "+str(self.destino)
		print "Contenido.", self.contenido
		
		
class Vecino:
	ref = 0
	key = 0
	
	def __init__(self,ref,key):
		self.ref = ref
		self.key = key
	
	def __get__(self, ref):
		return Vecino(ref)
	
	def __str__(self):
		print "Vecino:",self.ref,"a una distancia de:",self.key
		return ""

class Propuesta:
	ref = 0
	key = 0
	
	def __init__(self,ref,key):
		self.ref = ref
		self.key = key
		
	def __str__(self):
		print [self.ref, self.key]
		return ""

def sin_repetir(x):
	for i in range(len(x)):
		for j in range((len(x))):
			if (i!=j):
				if(x[i] == x[j]):
					aux = np.random.randint(0,20)
					if(aux not in x):
						x[i] = aux 
					else:
						while(aux in x):
							aux = np.random.randint(0,20)
						x[i] = aux
	return x	


def Inicializar_Robots(Lista, vector_x, vector_y):
	for i in range (10):
		R = Robot(i,vector_x[i],vector_y[i])
		Lista.append(R)
		G.add_node(i)
		#print R


def Plot_Kilobots(vector_x, vector_y, vector_id):
	plt.figure('Kilobots-Posicion') 
	N = len(vector_x)
	labels = ['Robot'+str(i)+" "+str([vector_x[i],vector_y[i]]).format(i) for i in range(N)]
	area = np.pi * 10**2
	plt.subplots_adjust(bottom = 0.1)
	for i in range(len(vector_x)):
		colors = np.random.rand(N)
		x = vector_x[i]
		y = vector_y[i]
		id = vector_id[i]
		plt.scatter(
    x, y, marker = 'o', c = colors, s = area, alpha = 0.5,
    cmap = plt.get_cmap('Spectral'))
		"""plt.scatter([x], [y], marker=str(id),
                        c="blue",
                        facecolors="white",
                        edgecolors="blue")"""
	#plt.scatter(x,y)
	for label, x, y in zip(labels, vector_x, vector_y):
		plt.annotate(
        label, 
        xy = (x, y), xytext = (-20, 20),
        textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

	plt.show()

def Plot_Grafo(Grafo):
	plt.figure('Grafo Kilobots')
	nx.draw_networkx_labels(G,pos = nx.spring_layout(G),font_size=20,font_family='sans-serif')
	nx.draw(G)
	plt.show()
	
	
def Descubrimiento(Lista):
	for i in range(len(Lista)):
		if(Lista[i].Sleep == 0):
			ListaRobots[i].Vecinos = Heap([])
			ListaRobots[i].Buzon = deque([])
			Buzon = ListaRobots[i].Buzon
			for j in range(len(Lista)):
				if(i!=j):
					if(abs(Lista[i].posicion_x- Lista[j].posicion_x)<10 and abs(Lista[i].posicion_y - Lista[j].posicion_y)<10 ):
						origen = j
						destino = i
						ref = j
						key = math.sqrt(((ListaRobots[i].posicion_x - ListaRobots[j].posicion_x)**2)+ abs((ListaRobots[i].posicion_y - ListaRobots[j].posicion_y)**2))
						contenido = [ref,key]
						G.add_edge(origen,destino,weight = key)
						mensaje = Mensaje(origen, destino, contenido)
						mensaje.send_message(Lista[j],Lista[i],contenido)
	"""
	IMPRIMIMOS LA LISTA DE ROBOTS CON LOS MENSAJES QUE HAN RECIBIDO
	for i in range(len(Lista)):
		print ListaRobots[i]"""
	for i in range(len(Lista)):
		Buzon = Lista[i].Buzon
		while(Buzon):
			msg = Mensaje(0,0,[])
			msg = msg.receive_message(ListaRobots[i])
			cont = msg.contenido
			ref = cont[0]
			key = cont[1]
			Vec = Vecino(ref,key)
			ListaRobots[i].Vecinos.__push__(Vec)


def Configuracion(Lista):
	for i in range(len(Lista)):
		if(Lista[i].Sleep == 0):
			Vecinos = Lista[i].Vecinos
			if(len(Vecinos)<2):
				#print "Se mueve el robot",Lista[i].identificador
				ListaRobots[i].__move__()
				Descubrimiento(Lista)
				Configuracion(Lista)	

def max(num1,num2):
	if(num1>=num2):
		return num1
	else:
		return num2

		
def Elegir_Equipo(ListaRobots):
	for i in range(len(ListaRobots)):
		if(ListaRobots[i].Sleep == 0):
			cont = 0
			Vecinos = ListaRobots[i].Vecinos
			ListaRobots[i].Propuestas = Heap([])
			#print "Robot: ", i
			for j in range(len(Vecinos)):
				for k in range(len(Vecinos)):
					if (j != k and k>j and ListaRobots[j].Sleep ==0 and ListaRobots[k].Sleep == 0):
						T_Propuestas[i].append([Vecinos[j].ref, Vecinos[k].ref,1, max(Vecinos[j].key,Vecinos[k].key),(10-max(Vecinos[j].key,Vecinos[k].key)*1)])
						ref = cont
						key = (10-max(Vecinos[j].key,Vecinos[k].key))*1
						Prop = Propuesta(ref,key)
						ListaRobots[i].Propuestas.__push__(Prop)
						#print "Propuesta anadida",[Vecinos[j].ref, Vecinos[k].ref,1, Vecinos[j].key+Vecinos[k].key,Vecinos[j].key+Vecinos[k].key*1]
						cont+=1
						Propuesta_Robot = [ListaRobots[i].identificador]
						Propuesta_Robot.append(T_Propuestas[ListaRobots[i].identificador][0][0])
						Propuesta_Robot.append(T_Propuestas[ListaRobots[i].identificador][0][1])
						ListaRobots[i].Propuesta_Robot = Propuesta_Robot

	
def Asignar_Roles(Lista):
	for i in range(len(Lista)):
		rol = random.randint(0,1)
		Lista[i].Rol = rol
		print "El rol del robot" ,i, " es: ", Lista[i].Rol 

def Enviar_Propuesta(Robot):
	Propuestas = Robot.Propuestas
	if(T_Propuestas[Robot.identificador]):
		Indice_Propuesta = Robot.Propuestas[0].ref
		Propuesta = [Robot.identificador]
		Propuesta_Inicial = T_Propuestas[Robot.identificador][Indice_Propuesta]
		Propuesta.append(Propuesta_Inicial[0])
		Propuesta.append(Propuesta_Inicial[1])
		msg = Mensaje(Robot.identificador,Propuesta[1],Propuesta)
		msg2 = Mensaje(Robot.identificador,Propuesta[2],Propuesta)
		Lista_Mensajes.append(msg)
		Lista_Mensajes.append(msg2)
		print "Robot ",Robot.identificador,"emite propuesta",Propuesta
	else:
		print "Robot ",Robot.identificador,"se quedo sin propuestas"
		Robot.__move__()
		Descubrimiento(ListaRobots)
		Configuracion(ListaRobots)
		Elegir_Equipo(ListaRobots)
		#Consenso(ListaRobots,Lista_Mensajes)
		
def Comparar(lista1, lista2):
	igual = 1
	for i in range(len(lista1)):
		if(lista1[i]==lista2[0] or lista1[i]==lista2[1] or lista1[i]==lista2[2]):
			i+=1
		else:
			igual = 0
	#if(igual == 1):
		# print lista1,lista2
	return igual

def ComprobarDormidos(Robot):
	Buzon = Robot.Buzon
	while(Buzon):
		mensaje = Buzon.pop()
		emisor = mensaje.origen
		contenido = mensaje.contenido
		if(contenido == [0,0,0]):
			print "Mensaje recibido de adios muy buenas emitido por", emisor, "para el robot", mensaje.destino
			Robot.Propuestas = Heap([])
			for i in range(len(T_Propuestas[Robot.identificador])-1,-1,-1):
				if(T_Propuestas[Robot.identificador][i][0] == emisor or T_Propuestas[Robot.identificador][i][1] == emisor):
					del T_Propuestas[Robot.identificador][i]
					Robot.Propuestas = Robot.Propuestas
			for i in range(len(T_Propuestas[Robot.identificador])):
				Propuesta_Nueva = Propuesta(i,T_Propuestas[Robot.identificador][i][4])
				Robot.Propuestas.__push__(Propuesta_Nueva)
			print "La nueva tabla de propuestas habiendose dormido", emisor, " es", T_Propuestas[Robot.identificador]
	
	
def Leer_Propuesta(Robot):
	#print "Tabla de Propuestas del robot",Robot.identificador,T_Propuestas[Robot.identificador]
	contador = 0
	meter_propuesta = 0
	ComprobarDormidos(Robot)
	for i in range(len(Lista_Mensajes)-1,-1,-1):
		if(Lista_Mensajes[i].destino == Robot.identificador):
			contador = contador +1
			#print Lista_Mensajes[i].destino, Robot.identificador
			mensaje = Lista_Mensajes[i]
			del Lista_Mensajes[i]
			contenido_mensaje = mensaje.contenido
			#print contenido_mensaje
			for j in range(len(Robot.Propuestas)):
				prop = [Robot.identificador]
				Propuesta = T_Propuestas[Robot.identificador][j]
				#print "Aqui esta",T_Propuestas[Robot.identificador][j]
				prop.append(Propuesta[0])
				prop.append(Propuesta[1])
				if(prop == [Robot.identificador]):
					Robot.__move__()
					Descubrimiento(ListaRobots)
					Configuracion(ListaRobots)
					Elegir_Equipo(ListaRobots)
					#Consenso(ListaRobots,Lista_Mensajes)
				igual = Comparar(contenido_mensaje, prop)
				#print igual
				if(igual == 1):
					ref = Robot.Propuestas[j].ref
					for i in range(len(Robot.Propuestas)):
						if(Robot.Propuestas[i].ref == ref):
							T_Propuestas[Robot.identificador][i][2]+=1
							print "Robot ",Robot.identificador,"lee propuesta",contenido_mensaje,"y suma"
						else:	
							if(T_Propuestas[Robot.identificador][i][2]>0):	
								T_Propuestas[Robot.identificador][i][2]-=1
								print "Robot ",Robot.identificador,"lee propuesta",contenido_mensaje,"y resta"
						T_Propuestas[Robot.identificador][i][4] = (10-T_Propuestas[Robot.identificador][i][3])*T_Propuestas[Robot.identificador][i][2]
		if(contador == 0):
			for i in range(len(T_Propuestas[Robot.identificador])):
				if(T_Propuestas[Robot.identificador][i][2]>0):	
					T_Propuestas[Robot.identificador][i][2]-=1	
					T_Propuestas[Robot.identificador][i][4] = (10-T_Propuestas[Robot.identificador][i][3])*T_Propuestas[Robot.identificador][i][2]			
	#print "La primera propuesta del Robot",Robot.identificador," es", T_Propuestas[Robot.identificador][0]

def Ordenar_Propuestas(Robot):
		while(Robot.Propuestas):
			Robot.Propuestas.__pop__()
		#print "ME QUEDO VACIO",Robot.Propuestas
		Propuestas_Completas = T_Propuestas[Robot.identificador]
		#print "Las propuestas completas", Propuestas_Completas
		for j in range(len(Propuestas_Completas)):
			#print "Una propuesta",Propuestas_Completas[j]
			NPropuesta = Propuestas_Completas[j]
			ref = j
			key = NPropuesta[4]
			Nueva_Propuesta = Propuesta(ref,-key)
			Robot.Propuestas.__push__(Nueva_Propuesta)
			#print Robot.Propuestas
		
		
def swap_roles(ListaR):
	for i in range(len(ListaR)):
		if(ListaR[i].Rol == 1):
			ListaR[i].Rol = 0
		else:
			ListaR[i].Rol = 1
		#print "El rol del robot",i ,"es", ListaR[i].Rol
			

def Convergencia(ListaRobots):
	for i in range(len(ListaRobots)):
		recuento = 0
		if(ListaRobots[i].Sleep == 0 and ListaRobots[i].Rol == 1):
			recuento = 1
			Vecinos = []
			propuesta = ListaRobots[i].Propuesta_Robot
			print "Robot",ListaRobots[i].identificador,"propone",propuesta
			for j in range(len(ListaRobots)):
				if(ListaRobots[j].Sleep == 0 and i!=j):
					propuesta2 = ListaRobots[j].Propuesta_Robot
					if(propuesta2 != []):
						if(Comparar(propuesta, propuesta2) == 1):
							recuento+=1
							Vecinos.append(j)
							print propuesta,propuesta2
		if(recuento == 3):
			R1 = ListaRobots[i]
			R1.Sleep = 1
			ListaRobots[Vecinos[0]].Sleep = 1
			ListaRobots[Vecinos[1]].Sleep = 1
			Vecinos_i = R1.Vecinos
			Vecinos_v1 = ListaRobots[Vecinos[0]].Vecinos
			Vecinos_v2 = ListaRobots[Vecinos[1]].Vecinos
			for i in range(len(Vecinos_i)):
				mensaje = Mensaje(R1.identificador,Vecinos_i[i].ref,[0,0,0])
				mensaje.send_message(R1,ListaRobots[Vecinos_i[i].ref],[0,0,0])
				print "Mensaje enviado de", R1.identificador," a ", Vecinos_i[i].ref
			for i in range(len(Vecinos_v1)):
				mensaje = Mensaje(ListaRobots[Vecinos[0]].identificador,Vecinos_v1[i].ref,[0,0,0])
				mensaje.send_message(ListaRobots[Vecinos[0]],ListaRobots[Vecinos_v1[i].ref],[0,0,0])
				print "Mensaje enviado de", ListaRobots[Vecinos[0]].identificador," a ", Vecinos_v1[i].ref
			for i in range(len(Vecinos_v2)):
				mensaje = Mensaje(ListaRobots[Vecinos[1]].identificador,Vecinos_v2[i].ref,[0,0,0])
				mensaje.send_message(ListaRobots[Vecinos[1]],ListaRobots[Vecinos_v2[i].ref],[0,0,0])
				print "Mensaje enviado de", ListaRobots[Vecinos[1]].identificador," a ", Vecinos_v2[i].ref
			print R1.identificador, ListaRobots[Vecinos[0]].identificador,ListaRobots[Vecinos[1]].identificador,"dormidos"
				
				
			
def Actualizar_Propuesta(Robot):
	longitud = len(Robot.Propuestas)
	if(longitud == 0):
		Robot.Propuesta_Robot = []
	else:
		ref = Robot.Propuestas[0].ref
		Propuesta = [Robot.identificador]
		Propuesta.append(T_Propuestas[Robot.identificador][ref][0])
	#	print "Otro elemento", T_Propuestas[Robot.identificador][0][0]
		Propuesta.append(T_Propuestas[Robot.identificador][ref][1])
	#	print "Otro elemento", T_Propuestas[Robot.identificador][0][1]
		Robot.Propuesta_Robot = Propuesta
	#	print "CAMBIO LA PROPUESTA",Robot.Propuesta_Robot

def Comparar_Parejas(ListaRobots):
	cont = 0
	Prop = []
	for i in range(len(ListaRobots)):
		Prop.append(ListaRobots[i].Propuesta_Robot)
		for j in range(len(Prop)):
			if(ListaRobots[i].Propuesta_Robot!= [] and Prop[j]!=[]):
				if(Comparar(ListaRobots[i].Propuesta_Robot,Prop[j]) == 1 and i!=j ):
					cont+=1
	if(cont/3 == 3):
		return 3
	else:
		return 0

def Consenso(ListaRobots, Lista_Mensajes):
	Asignar_Roles(ListaRobots)
	for i in range(Num_iteraciones):
		print "Iteracion",i
		if(i>10):
			print "Entro en la iteracion",i
			Convergencia(ListaRobots)
		for j in range(len(ListaRobots)):
			if(ListaRobots[j].Rol == 1 and ListaRobots[j].Sleep == 0):
				Enviar_Propuesta(ListaRobots[j])

		#print "La lista de mensajes es"
		#for i in range(len(Lista_Mensajes)):
			#print Lista_Mensajes[i].contenido
		#for i in range(len(Lista_Mensajes)):
		#	print Lista_Mensajes[i]

		for j in range(len(ListaRobots)):
			if(ListaRobots[j].Rol == 0 and ListaRobots[j].Sleep == 0):
				print "Lee robot",ListaRobots[j].identificador
				Leer_Propuesta(ListaRobots[j])
				Ordenar_Propuestas(ListaRobots[j])
				#print "Tabla actualizada ",ListaRobots[i].identificador,T_Propuestas[ListaRobots[i].identificador]
			Actualizar_Propuesta(ListaRobots[j])
		
		swap_roles(ListaRobots)	


def recuento(Votos):
	if(Votos[0] > Votos[1] and Votos[0] > Votos [2]):
		maximo = 0
	else:
		if (Votos[1] > Votos[0] and Votos[1] > Votos[2]):
			maximo = 1
		else:
			maximo = 2
	return maximo

def Seleccion_Lider(Propuestas):
	Lista_Prop = []
	for i in range(len(Propuestas)):
		Votos = [0,0,0]	
		for j in range(len(Propuestas[i])):
			voto = random.randint(1,10)
			if(voto > 5):
				print voto
				print "Voto a favor de", Propuestas[i][2]
				Votos[2] = Votos[2]+1
			else:
				print voto
				print "Voto a favor de", Propuestas[i][1]
				Votos[1] = Votos[1] + 1
		print Votos
		lider = recuento(Votos)
		print "El lider de la formacion", Propuestas[i], "es", Propuestas[i][lider]
		Lista_Prop.append(Propuestas[i])


G = nx.Graph()
ListaRobots = []
Lista_Mensajes = []
T_Propuestas = [[],[],[],[],[],[],[],[],[],[]]
Num_iteraciones = 100

def Pintar():
	id = []
	x = []
	y = []
	for i in range(len(ListaRobots)):
		x.append(int(ListaRobots[i].posicion_x))
		y.append(int(ListaRobots[i].posicion_y))
		id.append(ListaRobots[i].identificador)
	Plot_Kilobots(x,y,id)

vector = np.random.randint(0,20,(10,1))
x = sin_repetir(vector) 
y = np.random.randint(0,20,(10,1))

"""******************* INICIALIZACION KILOBOTS *******************"""
print "******************* INICIALIZACION KILOBOTS *******************"
#Inicializar_Robots(ListaRobots,x,y)
"""INICIALIZACION FORZADA"""
R0 = Robot(0,2,10)
ListaRobots.append(R0)
G.add_node(R0.identificador)
R1 = Robot(1,6,1)
ListaRobots.append(R1)
G.add_node(R1.identificador)
R2 = Robot(2,15,14)
ListaRobots.append(R2)
G.add_node(R2.identificador)
R3 = Robot(3,11,19)
ListaRobots.append(R3)
G.add_node(R3.identificador)
R4 = Robot(4,0,8)
ListaRobots.append(R4)
G.add_node(R4.identificador)
R5 = Robot(5,9,15)
ListaRobots.append(R5)
G.add_node(R5.identificador)
R6 = Robot(6,2,2)
ListaRobots.append(R6)
G.add_node(R6.identificador)
R7 = Robot(7,7,18)
ListaRobots.append(R7)
G.add_node(R7.identificador)
R8 = Robot(8,5,4)
ListaRobots.append(R8)
G.add_node(R8.identificador)
R9 = Robot(9,25,16)
ListaRobots.append(R9)
G.add_node(R9.identificador)
Pintar()

"""********************DESCUBRIMIENTO******************"""
print "********************DESCUBRIMIENTO******************"
Descubrimiento(ListaRobots)
#for i in range(len(ListaRobots)):
#	print ListaRobots[i]

Plot_Grafo(G)

Configuracion(ListaRobots)
Elegir_Equipo(ListaRobots)

#for i in range(len(ListaRobots)):
#	print ListaRobots[i]

Pintar()

print "********************CONSENSO Y CONFIGURACION DE EQUIPOS******************"
t0 = time.time()
t0min = int(t0/60)
num_iter = 0
print t0min
print "Y ahora",int(time.time()/60)
#while(((int(time.time()/60)-t0min))<3):
while(Comparar_Parejas(ListaRobots)!=3 or elapsed_time_minutes<1):
	print "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-."
	Consenso(ListaRobots,Lista_Mensajes)
	print "Termina la", num_iter ,"iteracion de consenso"
	print "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-."
	num_iter = num_iter + 1
	elapsed_time = int(time.time()-t0min)
	elapsed_time_minutes = int(elapsed_time/60)
cont = 0
Prop = []
for i in range(len(ListaRobots)):
	Prop.append(ListaRobots[i].Propuesta_Robot)
	for j in range(len(Prop)):
		if(ListaRobots[i].Propuesta_Robot!= [] and Prop[j]!=[]):
			if(Comparar(ListaRobots[i].Propuesta_Robot,Prop[j]) == 1 and i!=j ):
				cont = cont +1
	print "Robot",ListaRobots[i].identificador,":",ListaRobots[i].Propuesta_Robot
print "Se han garantizado",cont/3,"parejas"
Propuestas_Lider = [Prop[0]]
for i in range(len(Prop)-1):
	if(len(Prop[i]) == 0):
		del Prop[i]

for i in range(len(Prop)):
	cont = 0
	for j in range(len(Propuestas_Lider)):
		if(Comparar(Prop[i],Propuestas_Lider[j]) == 1):
			cont = cont +1
	if(cont == 0):
		Propuestas_Lider.append(Prop[i])


Pintar()
print "Propuestas:", Prop
print "Propuestas_Lider", Propuestas_Lider
Seleccion_Lider(Propuestas_Lider)



